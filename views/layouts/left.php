<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?? 'not authorized' ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?php /*
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        */ ?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Menu ', 'options' => ['class' => 'header']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Binpartner',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Common Stats', 'icon' => 'dashboard', 'url' => ['/binpartner-statistic/'],],
                            ['label' => 'Registrations', 'icon' => 'dashboard', 'url' => ['/binpartner-registration/'],],
                            ['label' => 'Accounts', 'icon' => 'file-code-o', 'url' => ['/binpartner-account/'],],
                            // ['label' => 'Create account', 'icon' => 'dashboard', 'url' => ['/binpartner-account/create'],],
                            /*[
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],*/
                        ],
                    ],
                    [
                        'label' => 'Taplink',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Click Stats', 'icon' => 'dashboard', 'url' => ['/taplink-click-statistic/'],],
                            ['label' => 'Taplink user profiles', 'icon' => 'dashboard', 'url' => ['/taplink-sub-account/'],],
                            ['label' => 'Accounts', 'icon' => 'file-code-o', 'url' => ['/taplink-account/'],],
                        ],
                    ],
                    [
                        'label' => 'Tiktok',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Involvement Stats', 'icon' => 'dashboard', 'url' => ['/tiktok-involvement-statistic/'],],
                            ['label' => 'Strim Stats', 'icon' => 'dashboard', 'url' => ['/tiktok-strim-statistic/'],],
                            ['label' => 'Accounts', 'icon' => 'file-code-o', 'url' => ['/tiktok-account/'],],
                        ],
                    ],
                    ['label' => 'Logs', 'icon' => 'dashboard', 'url' => ['/log/statistic-log']],
                    [
                        'label' => 'System',
                        'visible' => Yii::$app->request->get('dev') !== null,
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                            ['label' => 'Log', 'icon' => 'dashboard', 'url' => ['/log']],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
