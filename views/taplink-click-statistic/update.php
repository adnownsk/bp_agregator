<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\statistic\TaplinkClickStatistic */

$this->title = Yii::t('app', 'Update Taplink Click Statistic: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taplink Click Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="taplink-click-statistic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
