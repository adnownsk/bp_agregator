<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\statistic\TaplinkClickStatistic */

$this->title = Yii::t('app', 'Create Taplink Click Statistic');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taplink Click Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taplink-click-statistic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
