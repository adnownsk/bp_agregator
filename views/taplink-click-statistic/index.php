<?php

use app\widgets\PageSizeWidget;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use \app\models\taplink\subAccount\TaplinkSubAccountParams;

/* @var $this yii\web\View */
/* @var $searchModel app\models\taplink\statistic\TaplinkClickStatisticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Taplink Click Statistics');
$this->params['breadcrumbs'][] = $this->title;
$taplinkSubAccountList = TaplinkSubAccountParams::getActiveProAccountList();
?>
<div class="taplink-click-statistic-index">
    <?= PageSizeWidget::widget()?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);

    $columns = [
        'title',
        [
            'attribute' => 'taplink_sub_account_params_id',
            'label' => 'Profile ',
            'filter' => $taplinkSubAccountList,
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data->taplinkSubAccountParams->link, Url::to(['/taplink-sub-account/view', 'id' => $data->taplink_sub_account_params_id]), ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'date',
            'format' => ['date', 'php:d.m.Y'],
            'filter' => DateRangePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'php:d.m.Y',
                        'locale' => ['format' => 'Y-m-d'],
                        'todayHighlight' => true
                    ]
                ]
            ),
            'filterOptions' => [
                'type' => 'date',
                'style' => 'min-width:140px',
            ],
        ],
        'block_id',
        //'slot_id',
        'clicks',
        [
            'attribute' => 'cv',
            'value' => function ($data) {
                return $data->cv . '%';
            }
        ],
        'type',
        //'created_at',
        /*[
            'class' => ActionColumn::className(),
            'urlCreator' => function ($action, TaplinkClickStatistic $model, $key, $index, $column) {
                return Url::toRoute([$action, 'id' => $model->id]);
             }
        ],*/
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'clearBuffers' => true,
        'exportConfig' => [
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
        ],
        'filename' => 'taplink_click_stats_data('.date('d-M-Y').')'
    ]);
    ?>


    <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>


</div>
