<?php

use app\models\binpartner\statistic\BinpartnerStatistic;
use app\widgets\PageSizeWidget;
use kartik\daterange\DateRangePicker;
use \yii\helpers\Html;
use \yii\helpers\Url;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use \app\models\binpartner\account\BinpartnerAccountParams;

/* @var $this yii\web\View */
/* @var $searchModel app\models\binpartner\statistic\BinpartnerStatisticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Binpartner Statistics');
$this->params['breadcrumbs'][] = $this->title;
$binpartnerAccountList = BinpartnerAccountParams::getActiveAccountList();
?>
<div class="binpartner-statistic-index">
    <?= PageSizeWidget::widget()?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);


    $columns = [
        [
            'attribute' => 'binpartner_account_params_id',
            'label' => 'Email ',
            'filter' => $binpartnerAccountList,
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data->binpartnerAccountParams->email, Url::to(['/binpartner-account/view', 'id' => $data->binpartner_account_params_id]), ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'day',
            'format' => ['date', 'php:d.m.Y'],
            'filter' => DateRangePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'day',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'php:d.m.Y',
                        'locale' => ['format' => 'Y-m-d'],
                        'todayHighlight' => true
                    ]
                ]
            ),
            'filterOptions' => [
                'type' => 'date',
                'style' => 'min-width:140px',
            ],
        ],
        'visitors',
        'reg',
        'conf',
        [
            'attribute' => 'ctr',
            'value' => function ($data) {
                return $data->ctr . '%';
            }
        ],
        'demo',
        'ftd',
        [
            'attribute' => 'ftds',
            'value' => function ($data) {
                return $data->ftds . '$';
            }
        ],
        [
            'attribute' => 'ratio',
            'value' => function ($data) {
                return $data->first_ratio . '%/' . $data->sign_up_ratio . '%';
            }
        ],
        'deposits_count',
        [
            'attribute' => 'deposits',
            'value' => function ($data) {
                return $data->deposits . '$';
            }
        ],
        'traders',
        'deals_count',
        [
            'attribute' => 'deals',
            'value' => function ($data) {
                return $data->deals . '$';
            }
        ],
        [
            'attribute' => 'flows',
            'value' => function ($data) {
                return $data->flows . '$';
            }
        ],
        [
            'attribute' => 'withs',
            'value' => function ($data) {
                return $data->withs . '$';
            }
        ],
        [
            'attribute' => 'profit',
            'value' => function ($data) {
                return $data->profit . '$';
            }
        ],
        /*[
            'class' => ActionColumn::className(),
            'urlCreator' => function ($action, BinpartnerStatistic $model, $key, $index, $column) {
                return Url::toRoute([$action, 'id' => $model->id]);
             }
        ],*/
    ];
    ?>

    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'clearBuffers' => true,
        'exportConfig' => [
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_HTML => false,
        ],
        'filename' => 'binpartner_common_stats_data('.date('d-M-Y').')'
    ]);
    ?>

    <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>


</div>
