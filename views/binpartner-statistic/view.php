<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model models\binpartner\statistic\BinpartnerStatistic */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Binpartner Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="binpartner-statistic-view">


    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'binpartner_account_params_id',
            'day',
            'visitors',
            'reg',
            'conf',
            'ctr',
            'demo',
            'ftd',
            'ftds',
            'first_ratio',
            'sign_up_ratio',
            'deposits_count',
            'deposits',
            'traders',
            'deals_count',
            'deals',
            'flows',
            'withs',
            'profit',
        ],
    ]) ?>

</div>
