<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model models\binpartner\statistic\BinpartnerStatisticSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="binpartner-statistic-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'binpartner_account_params_id') ?>

    <?= $form->field($model, 'day') ?>

    <?= $form->field($model, 'visitors') ?>

    <?= $form->field($model, 'reg') ?>

    <?php // echo $form->field($model, 'conf') ?>

    <?php // echo $form->field($model, 'ctr') ?>

    <?php // echo $form->field($model, 'demo') ?>

    <?php // echo $form->field($model, 'ftd') ?>

    <?php // echo $form->field($model, 'ftds') ?>

    <?php // echo $form->field($model, 'first_ratio') ?>

    <?php // echo $form->field($model, 'sign_up_ratio') ?>

    <?php // echo $form->field($model, 'deposits_count') ?>

    <?php // echo $form->field($model, 'deposits') ?>

    <?php // echo $form->field($model, 'traders') ?>

    <?php // echo $form->field($model, 'deals_count') ?>

    <?php // echo $form->field($model, 'deals') ?>

    <?php // echo $form->field($model, 'flows') ?>

    <?php // echo $form->field($model, 'withs') ?>

    <?php // echo $form->field($model, 'profit') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
