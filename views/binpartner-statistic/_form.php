<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model models\binpartner\statistic\BinpartnerStatistic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="binpartner-statistic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'binpartner_account_params_id')->textInput() ?>

    <?= $form->field($model, 'day')->textInput() ?>

    <?= $form->field($model, 'visitors')->textInput() ?>

    <?= $form->field($model, 'reg')->textInput() ?>

    <?= $form->field($model, 'conf')->textInput() ?>

    <?= $form->field($model, 'ctr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'demo')->textInput() ?>

    <?= $form->field($model, 'ftd')->textInput() ?>

    <?= $form->field($model, 'ftds')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_ratio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sign_up_ratio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deposits_count')->textInput() ?>

    <?= $form->field($model, 'deposits')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'traders')->textInput() ?>

    <?= $form->field($model, 'deals_count')->textInput() ?>

    <?= $form->field($model, 'deals')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'flows')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'withs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'profit')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
