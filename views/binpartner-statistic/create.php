<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model models\binpartner\statistic\BinpartnerStatistic */

$this->title = Yii::t('app', 'Create Binpartner Statistic');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Binpartner Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="binpartner-statistic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
