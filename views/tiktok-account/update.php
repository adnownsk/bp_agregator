<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\account\TiktokAccountParams */

$this->title = Yii::t('app', 'Update Tiktok Account Params: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiktok Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tiktok-account-params-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
