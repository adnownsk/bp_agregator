<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\account\TiktokAccountParams */

$this->title = Yii::t('app', 'Create Tiktok Account Params');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiktok Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiktok-account-params-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
