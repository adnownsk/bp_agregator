<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\account\TiktokAccountParams */
/* @var $form yii\widgets\ActiveForm */

$helpText = 'Для заполнения, логинимся под учеткой в <span style="color: red"> АНОНИМНОЙ ВКЛАДКЕ</span>. Открываем панель разработчика (клавиша F12), 
выбираем Application => Storage => Cookies => https://www.tiktok.com . Находим в открывшемся списке под столбцом NAME - sessionid, и копируем строку 
из столбца VALUE для этой записи. <span style="color: red"> Закрываем браузер. кнопку LOGOUT не нажимать!</span>';
?>

<div class="tiktok-account-params-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'session_id')->textInput(['maxlength' => true])->hint($helpText) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(app\models\tiktok\account\TiktokAccountParams::getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
