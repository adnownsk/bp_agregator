<?php

use app\models\tiktok\account\TiktokAccountParams;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\tiktok\account\TiktokAccountParamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tiktok Account Params');
$this->params['breadcrumbs'][] = $this->title;
$statusList = TiktokAccountParams::getStatusList();
?>
<div class="tiktok-account-params-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Tiktok Account Params'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //'session_id',
            'username',
            'email',
            //'password',
            [
                'attribute' => 'status',
                'filter' => $statusList,
                'value' => function ($data) use ($statusList) {
                    return ArrayHelper::getValue($statusList, $data->status);
                }
            ],
            //'created_at',
            [
                'attribute' => 'expired_at',
                'format' => ['date', 'php:d.m.Y'],
                'filter'=>DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'expired_at',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y'],
                'filter'=>DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'updated_at',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, TiktokAccountParams $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
