<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\subAccount\TaplinkSubAccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taplink-sub-account-params-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'taplink_account_params_id') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'is_completed') ?>

    <?= $form->field($model, 'page_id') ?>

    <?= $form->field($model, 'profile_status_id') ?>

    <?php // echo $form->field($model, 'tariff_current') ?>

    <?php // echo $form->field($model, 'is_favourite') ?>

    <?php // echo $form->field($model, 'is_transfer_pending') ?>

    <?php // echo $form->field($model, 'profile_id') ?>

    <?php // echo $form->field($model, 'link') ?>

    <?php // echo $form->field($model, 'tms_created') ?>

    <?php // echo $form->field($model, 'tariff_until') ?>

    <?php // echo $form->field($model, 'tariff') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'access_token') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'password') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
