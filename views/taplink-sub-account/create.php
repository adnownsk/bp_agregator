<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\subAccount\TaplinkSubAccountParams */

$this->title = Yii::t('app', 'Create Taplink Sub Account Params');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taplink Sub Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taplink-sub-account-params-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
