<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\subAccount\TaplinkSubAccountParams */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taplink Sub Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="taplink-sub-account-params-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'taplink_account_params_id',
            'is_completed',
            'page_id',
            'profile_status_id',
            'tariff_current',
            'is_favourite',
            'email',
            'is_transfer_pending',
            'profile_id',
            'link',
            'tms_created',
            'tariff_until',
            'tariff',
            'created_at',
            'updated_at',
            'access_token',
            'email:email',
            'password',
        ],
    ]) ?>

</div>
