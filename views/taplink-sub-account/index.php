<?php

use app\models\taplink\account\TaplinkAccountParams;
use app\models\taplink\subAccount\TaplinkSubAccountParams;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\taplink\subAccount\TaplinkSubAccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Taplink user profiles');
$this->params['breadcrumbs'][] = $this->title;
$statusList = \app\models\taplink\subAccount\TaplinkSubAccountParams::getStatusList();
$taplinkAccountList = TaplinkAccountParams::getActiveAccountList();
$taplinkLinkList = \app\models\taplink\subAccount\TaplinkSubAccountParams::getParamList('link');
$favouriteList = [0 => 'not favourite', 1 => 'favourite'];
?>
<div class="taplink-sub-account-params-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'taplink_account_params_id',
                'label' => 'Account ',
                'filter' => $taplinkAccountList,
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->taplinkAccountParams->email, Url::to(['/taplink-account/view', 'id' => $data->taplink_account_params_id]), ['target' => '_blank']);
                }
            ],
            'email',
            [
                'attribute' => 'link',
                'label' => 'Link ',
                'filter' => $taplinkLinkList,
                'format' => 'raw',
            ],
            [
                'attribute' => 'tariff',
                'filter' => \app\models\taplink\subAccount\TaplinkSubAccountParams::getParamList('tariff'),
                'format' => 'raw',
            ],
            [
                'attribute' => 'tariff_current',
                'filter' => \app\models\taplink\subAccount\TaplinkSubAccountParams::getParamList('tariff_current'),
                'format' => 'raw',
            ],
            [
                'attribute' => 'tms_created',
                'format' => ['date', 'php:d.m.Y'],
                'filter' => DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'tms_created',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            [
                'attribute' => 'tariff_until',
                'format' => ['date', 'php:d.m.Y'],
                'filter' => DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'tariff_until',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            //'is_completed',
            //'page_id',
            'profile_status_id',
            [
                'attribute' => 'is_favourite',
                'format' => 'raw',
                'filter' => $favouriteList,
                'value' => function ($data) use ($favouriteList) {
                    return $favouriteList[$data->is_favourite];
                }
            ],
            //'is_transfer_pending',
            'profile_id',
            [
                'attribute' => 'status',
                'filter' => $statusList,
                'value' => function ($data) use ($statusList) {
                    return ArrayHelper::getValue($statusList, $data->status);
                }
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y'],
                'filter' => DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'updated_at',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{hide}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'account-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'account-update'),
                        ]);
                    },

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='/taplink-sub-account/view?id='.$model->id;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url ='/taplink-sub-account/update?id='.$model->id;
                        return $url;
                    }
                }
            ],

            /*
             [
                 'class' => ActionColumn::className(),
                 'urlCreator' => function ($action, TaplinkSubAccountParams $model, $key, $index, $column) {
                     return Url::toRoute([$action, 'id' => $model->id]);
                  }
             ],
            */
        ],
    ]); ?>


</div>
