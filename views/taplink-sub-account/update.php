<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\subAccount\TaplinkSubAccountParams */

$this->title = Yii::t('app', 'Update Taplink Sub Account Params: {name}', [
    'name' => $model->link,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taplink Sub Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="taplink-sub-account-params-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
