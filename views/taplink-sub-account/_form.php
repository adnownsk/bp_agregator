<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\subAccount\TaplinkSubAccountParams */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taplink-sub-account-params-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>
    <?php /*
    <?= $form->field($model, 'taplink_account_params_id')->textInput() ?>

    <?= $form->field($model, 'is_completed')->textInput() ?>

    <?= $form->field($model, 'page_id')->textInput() ?>

    <?= $form->field($model, 'profile_status_id')->textInput() ?>

    <?= $form->field($model, 'tariff_current')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_favourite')->textInput() ?>

    <?= $form->field($model, 'is_transfer_pending')->textInput() ?>

    <?= $form->field($model, 'profile_id')->textInput() ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tms_created')->textInput() ?>

    <?= $form->field($model, 'tariff_until')->textInput() ?>

    <?= $form->field($model, 'tariff')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

*/?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
