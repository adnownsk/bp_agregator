<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\binpartner\registration\BinpartnerRegistration */

$this->title = Yii::t('app', 'Create Binpartner Registration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Binpartner Registrations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="binpartner-registration-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
