<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\binpartner\registration\BinpartnerRegistration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="binpartner-registration-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'binpartner_account_params_id')->textInput() ?>

    <?= $form->field($model, 'trader_id')->textInput() ?>

    <?= $form->field($model, 'campaign')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subaccount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registration_date')->textInput() ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
