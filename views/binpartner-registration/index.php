<?php

use app\models\binpartner\account\BinpartnerAccountParams;
use app\widgets\PageSizeWidget;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use \app\models\binpartner\registration\BinpartnerRegistration;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\binpartner\registration\BinpartnerRegistrationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Binpartner Registrations');
$this->params['breadcrumbs'][] = $this->title;
$binpartnerAccountList = BinpartnerAccountParams::getActiveAccountList();
?>
<div class="binpartner-registration-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= PageSizeWidget::widget() ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);
    $columns = [
        /*['class' => 'yii\grid\SerialColumn'],*/

        //'id',
        [
            'attribute' => 'binpartner_account_params_id',
            'label' => 'Email ',
            'filter' => $binpartnerAccountList,
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data->binpartnerAccountParams->email, Url::to(['/binpartner-account/view', 'id' => $data->binpartner_account_params_id]), ['target' => '_blank']);
            }
        ],
        'trader_id',
        'campaign',
        'subaccount',
        [
            'attribute' => 'registration_date',
            'format' => ['date', 'php:d.m.Y  H:i'],
            'filter' => DateRangePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'registration_date',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'php:d.m.Y',
                        'locale' => ['format' => 'Y-m-d'],
                        'todayHighlight' => true
                    ]
                ]
            ),
            'filterOptions' => [
                'type' => 'date',
                'style' => 'min-width:140px',
            ],
        ],
        [
            'attribute' => 'country',
            'filter' => BinpartnerRegistration::getParamList('country'),
        ],
        //'created_at',
        /*[
            'class' => ActionColumn::className(),
            'urlCreator' => function ($action, BinpartnerRegistration $model, $key, $index, $column) {
                return Url::toRoute([$action, 'id' => $model->id]);
             }
        ],*/
    ];

    ?>

    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'clearBuffers' => true,
        'exportConfig' => [
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
        ],
        'filename' => 'binpartner_registration_data(' . date('d-M-Y') . ')'
    ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns
    ]); ?>


</div>
