<?php

use yii\grid\GridView;
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;

/**
 * @var $this         yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Yii::t('app', 'System Logs');

$this->params['breadcrumbs'][] = $this->title;

$script = "
        function setParams(){
            var keyList = $('#grid').yiiGridView('getSelectedRows');
            if(keyList != '') {
                $('#btn-multi-delete').attr('data-params', JSON.stringify({keyList}));
            } else {
                $('#btn-multi-delete').removeAttr('data-params');
            }
        };";
$this->registerJs($script, yii\web\View::POS_BEGIN);


?>

<p>
    <?php echo Html::a(Yii::t('app', 'Clear'), false, ['class' => 'btn btn-danger', 'data-method' => 'delete']) ?>
</p>

<?php echo GridView::widget([
    'id' => 'grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'layout' => "{pager}\n{summary}\n{items}\n{summary}\n{pager}",
    'options' => [
        'class' => 'grid-view table-responsive',
    ],
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'contentOptions' => ['class' => 'grid-checkbox'],
        ],
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [
            'attribute' => 'level',
            'value' => function ($model) {
                return \yii\log\Logger::getLevelName($model->level);
            },
            'filter' => [
                \yii\log\Logger::LEVEL_ERROR => 'error',
                \yii\log\Logger::LEVEL_WARNING => 'warning',
                \yii\log\Logger::LEVEL_INFO => 'info',
                \yii\log\Logger::LEVEL_TRACE => 'trace',
                \yii\log\Logger::LEVEL_PROFILE_BEGIN => 'profile begin',
                \yii\log\Logger::LEVEL_PROFILE_END => 'profile end',
            ],
        ],
        'category',
        [
            'attribute' => 'prefix',
            'filter' => \app\models\systemLog\SystemLog::getPrefix(),
        ],
        'message',
        [
            'attribute' => 'log_time',
            'format' => ['date', 'php:d.m.Y H:i'],
            'filter' => \kartik\date\DatePicker::widget([
                'name' => 'SystemLogSearch[log_time]',
                'model' => $searchModel,
            ])
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {delete}',
        ],
    ],
]); ?>

<?= Html::a(Yii::t('app', 'Delete selected'),
    ['multi-delete'], [
        'id' => 'btn-multi-delete',
        'class' => 'btn btn-default',
        'onclick' => 'setParams()',
        'data' => [
            'method' => 'post',
            'type' => 'delete'
        ]
    ]); ?>
