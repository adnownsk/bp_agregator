<?php

use app\models\binpartner\account\BinpartnerAccountParams;
use yii\helpers\Html;
use \kartik\daterange\DateRangePicker;
use yii\grid\GridView;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\binpartner\account\BinpartnerAccountParamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Binpartner Account Params');
$this->params['breadcrumbs'][] = $this->title;
$statusList = BinpartnerAccountParams::getStatusList();

?>
<div class="binpartner-account-params-index">

    <p>
        <?= Html::a(Yii::t('app', 'Add Binpartner Account'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'filterOptions' => [
                    'style' => 'max-width:50px',
                ],
            ],
            'email:email',
            [
                'attribute' => 'status',
                'filter' => $statusList,
                'value' => function ($data) use ($statusList) {
                    return ArrayHelper::getValue($statusList, $data->status);
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
                'filter'=>DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y'],
                'filter'=>DateRangePicker::widget(
                        [
                            'model' => $searchModel,
                            'attribute' => 'updated_at',
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'format' => 'php:d.m.Y',
                                'locale' => ['format' => 'Y-m-d'],
                                'todayHighlight' => true
                            ]
                        ]
                ),
                'filterOptions' => [
                        'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            'balance',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{hide}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'account-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'account-update'),
                        ]);
                    },
                    'hide' => function ($url, $model) {
                        if($model->status === BinpartnerAccountParams::STATUS_DISABLE){
                            return false;
                        }
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                            'title' => Yii::t('app', 'account-hide'),
                        ]);
                    }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='binpartner-account/view?id='.$model->id;
                        return $url;
                    }

                    if ($action === 'update') {
                        $url ='binpartner-account/update?id='.$model->id;
                        return $url;
                    }
                    if ($action === 'hide') {
                        $url ='binpartner-account/set-disable-status?id='.$model->id;
                        return $url;
                    }
                }
            ],

        ],
    ]); ?>


</div>
