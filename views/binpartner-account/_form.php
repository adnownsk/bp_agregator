<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\binpartner\account\BinpartnerAccountParams */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="binpartner-account-params-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'access_token')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(app\models\binpartner\account\BinpartnerAccountParams::getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
