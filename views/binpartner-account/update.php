<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\binpartner\account\BinpartnerAccountParams */

$this->title = Yii::t('app', 'Update Binpartner Account Params: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Binpartner Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="binpartner-account-params-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
