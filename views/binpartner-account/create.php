<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\binpartner\account\BinpartnerAccountParams */

$this->title = Yii::t('app', 'Create Binpartner Account Params');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Binpartner Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="binpartner-account-params-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
