<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\statistic\TiktokStrimStatisticSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiktok-strim-statistic-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tiktok_account_params_id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'user_live_views') ?>

    <?= $form->field($model, 'user_live_unique_viewers') ?>

    <?php // echo $form->field($model, 'user_live_diamonds') ?>

    <?php // echo $form->field($model, 'user_live_gift_unique_viewers') ?>

    <?php // echo $form->field($model, 'user_live_new_followers') ?>

    <?php // echo $form->field($model, 'user_live_duration_time') ?>

    <?php // echo $form->field($model, 'user_live_top_viewers') ?>

    <?php // echo $form->field($model, 'user_live_follower_diamonds') ?>

    <?php // echo $form->field($model, 'user_live_cnt') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
