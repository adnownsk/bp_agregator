<?php

use app\widgets\PageSizeWidget;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\tiktok\statistic\TiktokStrimStatisticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tiktok Strim Statistics');
$this->params['breadcrumbs'][] = $this->title;
$tiktokAccountList = \app\models\tiktok\account\TiktokAccountParams::getActiveAccountList();
?>
<div class="tiktok-strim-statistic-index">

    <?= PageSizeWidget::widget()?>
    <?php
    $columns =[
        [
            'attribute' => 'tiktok_account_params_id',
            'label' => 'Profile ',
            'filter' => $tiktokAccountList,
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data->tiktokAccountParams->username, Url::to(['/tiktok-account/view', 'id' => $data->tiktok_account_params_id]), ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'date',
            'format' => ['date', 'php:d.m.Y'],
            'filter' => DateRangePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'php:d.m.Y',
                        'locale' => ['format' => 'Y-m-d'],
                        'todayHighlight' => true
                    ]
                ]
            ),
            'filterOptions' => [
                'type' => 'date',
                'style' => 'min-width:140px',
            ],
        ],
        'live_views',
        'live_unique_viewers',
        'live_diamonds',
        'live_gift_unique_viewers',
        'live_new_followers',
        'live_duration_time',
        'live_top_viewers',
        'live_follower_diamonds',
        'live_cnt',
        [
            'attribute' => 'updated_at',
            'format' => ['date', 'php:d.m.Y'],
            'filter'=>DateRangePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'format' => 'php:d.m.Y',
                        'locale' => ['format' => 'Y-m-d'],
                        'todayHighlight' => true
                    ]
                ]
            ),
            'filterOptions' => [
                'type' => 'date',
                'style' => 'min-width:140px',
            ],
        ],
        //'created_at',
        /*[
            'class' => ActionColumn::className(),
            'urlCreator' => function ($action, TiktokStrimStatistic $model, $key, $index, $column) {
                return Url::toRoute([$action, 'id' => $model->id]);
            }
        ],*/
    ];
    ?>
    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'clearBuffers' => true,
        'exportConfig' => [
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
        ],
        'filename' => 'tiktok_strim_stats_data('.date('d-M-Y').')'
    ]);
    ?>


    <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>

</div>
