<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\statistic\TiktokStrimStatistic */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiktok Strim Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tiktok-strim-statistic-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tiktok_account_params_id',
            'date',
            'user_live_views',
            'user_live_unique_viewers',
            'user_live_diamonds',
            'user_live_gift_unique_viewers',
            'user_live_new_followers',
            'user_live_duration_time:datetime',
            'user_live_top_viewers',
            'user_live_follower_diamonds',
            'user_live_cnt',
            'updated_at',
            'created_at',
        ],
    ]) ?>

</div>
