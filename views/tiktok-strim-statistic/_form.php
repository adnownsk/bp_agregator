<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\statistic\TiktokStrimStatistic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiktok-strim-statistic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tiktok_account_params_id')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'user_live_views')->textInput() ?>

    <?= $form->field($model, 'user_live_unique_viewers')->textInput() ?>

    <?= $form->field($model, 'user_live_diamonds')->textInput() ?>

    <?= $form->field($model, 'user_live_gift_unique_viewers')->textInput() ?>

    <?= $form->field($model, 'user_live_new_followers')->textInput() ?>

    <?= $form->field($model, 'user_live_duration_time')->textInput() ?>

    <?= $form->field($model, 'user_live_top_viewers')->textInput() ?>

    <?= $form->field($model, 'user_live_follower_diamonds')->textInput() ?>

    <?= $form->field($model, 'user_live_cnt')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
