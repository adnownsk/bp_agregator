<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\statistic\TiktokInvolvementStatistic */

$this->title = Yii::t('app', 'Update Tiktok Statistic: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiktok Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tiktok-statistic-update">
<?php /*
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
*/?>
</div>
