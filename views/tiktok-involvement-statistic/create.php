<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\statistic\TiktokInvolvementStatistic */

$this->title = Yii::t('app', 'Create Tiktok Statistic');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiktok Statistics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiktok-statistic-create">
<?php /*
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    */?>
</div>
