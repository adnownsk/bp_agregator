<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\statistic\TiktokInvolvementStatistic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiktok-statistic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tiktok_account_params_id')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>


    <?= $form->field($model, 'vv')->textInput() ?>

    <?= $form->field($model, 'pv')->textInput() ?>

    <?= $form->field($model, 'like_history')->textInput() ?>

    <?= $form->field($model, 'comment')->textInput() ?>

    <?= $form->field($model, 'share')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
