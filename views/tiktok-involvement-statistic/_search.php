<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tiktok\statistic\TiktokInvolvementStatisticSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiktok-statistic-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tiktok_account_params_id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'vv') ?>

    <?php // echo $form->field($model, 'pv') ?>

    <?php // echo $form->field($model, 'like') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'share') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
