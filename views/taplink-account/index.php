<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use \app\models\taplink\account\TaplinkAccountParams;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\taplink\account\TaplinkAccountParamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Taplink Account');
$this->params['breadcrumbs'][] = $this->title;
$statusList = TaplinkAccountParams::getStatusList();
?>
<div class="taplink-account-params-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Taplink Account'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'email:email',
            'country',
            'tariff',
            [
                'attribute' => 'tariff_until',
                'format' => ['date', 'php:d.m.Y'],
                'filter'=>DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'tariff_until',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            'nickname',
            //'username',
            'domain',
            'profile_id',
            'session_account_id',
            'partner_id',
            //'invite_partner_id',
            //'appsumo_user_id',
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y'],
                'filter'=>DateRangePicker::widget(
                    [
                        'model' => $searchModel,
                        'attribute' => 'updated_at',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'format' => 'php:d.m.Y',
                            'locale' => ['format' => 'Y-m-d'],
                            'todayHighlight' => true
                        ]
                    ]
                ),
                'filterOptions' => [
                    'type' => 'date',
                    'style' => 'min-width:140px',
                ],
            ],
            //'user_id',
            //'account_id',
            //'has_nickname',
            //'plan_paid',
            //'utc_timezone',
            //'id',
            //'password',
            //'session',
            [
                'attribute' => 'status',
                'filter' => $statusList,
                'value' => function ($data) use ($statusList) {
                    return ArrayHelper::getValue($statusList, $data->status);
                }
            ],
            'tips_bits',
            //'created_at',
            //'tariff_ends',
            //'tariff_ends_days',
            //'trial_tariff',
            //'trial_activated',
            //'domain_id',
            //'hash',
            //'favourites',
            //'access',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{hide}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'account-view'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'account-update'),
                        ]);
                    },
                    'hide' => function ($url, $model) {
                        if($model->status === TaplinkAccountParams::STATUS_DISABLE){
                            return false;
                        }
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                            'title' => Yii::t('app', 'account-hide'),
                        ]);
                    }

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='taplink-account/view?id='.$model->id;
                        return $url;
                    }

                    if ($action === 'update') {
                        $url ='taplink-account/update?id='.$model->id;
                        return $url;
                    }
                    if ($action === 'hide') {
                        $url ='taplink-account/set-disable-status?id='.$model->id;
                        return $url;
                    }
                }
            ],

            /* [
                 'class' => ActionColumn::className(),
                 'urlCreator' => function ($action, TaplinkAccountParams $model, $key, $index, $column) {
                     return Url::toRoute([$action, 'id' => $model->id]);
                  }
             ],*/
        ],
    ]); ?>


</div>
