<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\account\TaplinkAccountParams */

$this->title = Yii::t('app', 'Create Taplink Account Params');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taplink Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taplink-account-params-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
