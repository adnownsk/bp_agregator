<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\account\TaplinkAccountParamsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taplink-account-params-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'password') ?>

    <?= $form->field($model, 'session') ?>

    <?= $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'plan_paid') ?>

    <?php // echo $form->field($model, 'tariff') ?>

    <?php // echo $form->field($model, 'tariff_ends') ?>

    <?php // echo $form->field($model, 'tariff_ends_days') ?>

    <?php // echo $form->field($model, 'tariff_until') ?>

    <?php // echo $form->field($model, 'trial_tariff') ?>

    <?php // echo $form->field($model, 'trial_activated') ?>

    <?php // echo $form->field($model, 'username') ?>

    <?php // echo $form->field($model, 'domain') ?>

    <?php // echo $form->field($model, 'domain_id') ?>

    <?php // echo $form->field($model, 'profile_id') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'account_id') ?>

    <?php // echo $form->field($model, 'session_account_id') ?>

    <?php // echo $form->field($model, 'hash') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'favourites') ?>

    <?php // echo $form->field($model, 'partner_id') ?>

    <?php // echo $form->field($model, 'invite_partner_id') ?>

    <?php // echo $form->field($model, 'has_nickname') ?>

    <?php // echo $form->field($model, 'tips_bits') ?>

    <?php // echo $form->field($model, 'utc_timezone') ?>

    <?php // echo $form->field($model, 'nickname') ?>

    <?php // echo $form->field($model, 'access') ?>

    <?php // echo $form->field($model, 'appsumo_user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
