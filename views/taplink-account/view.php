<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\taplink\account\TaplinkAccountParams */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taplink Account Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="taplink-account-params-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email:email',
            'nickname',
            //'password',
            //'session',
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    return ArrayHelper::getValue(\app\models\taplink\account\TaplinkAccountParams::getStatusList(), $data->status);
                }
            ],
            'country',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y'],
            ],
            'plan_paid',
            //'tariff',
            //'tariff_ends',
            'tariff_ends_days',
            [
                'attribute' => 'tariff_until',
                'format' => ['date', 'php:d.m.Y'],
            ],
            'trial_tariff',
            'trial_activated',
            //'username',
            'domain',
            //'domain_id',
            'profile_id',
            //'user_id',
            'account_id',
            'session_account_id',
            //'hash',
            'favourites',
            //'partner_id',
            //'invite_partner_id',
            //'has_nickname',
            'tips_bits',
            'utc_timezone',
          //  'access',
           // 'appsumo_user_id',
        ],
    ]) ?>

</div>
