<?php

namespace app\service\taplink;

class SubUserReaderService extends AbstractStatisticReaderService
{
    /**
     * @param array $params
     * @return array|bool
     */
    public function getSubUsers($profile_id)
    {
        return $this->getSubUsersData($profile_id);
    }
}