<?php

namespace app\service\taplink;

class StatisticReaderService extends AbstractStatisticReaderService
{

    /**
     * @param array $params
     * @return array|bool
     */
    public function getStatisticData($profile_id, $pageId, $periodBack = 0)
    {
        return $this->getSubUserBatch($profile_id, $pageId,$periodBack);

    }
}