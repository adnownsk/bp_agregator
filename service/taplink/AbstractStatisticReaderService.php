<?php

namespace app\service\taplink;

use app\models\taplink\statistic\TaplinkClickStatistic;
use app\service\systemLog\SystemLogService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;
use yii\log\Logger;

abstract class AbstractStatisticReaderService
{
    public const TRANSPORT_ID = 1;
    public const METHOT_POST = 'post';
    public const METHOT_GET = 'get';
    public const TAPLINK_LOGIN_URL = 'auth/login.json';
    public const TAPLINK_BATCH_URL = 'batch.json';
    public const TAPLINK_USER_PROFILES_URL = 'account/profiles/list.json';

    protected $_httpClient;
    protected $apiUrl;
    protected $session;
    protected $user;
    protected $authentication;


    /**
     * AbstractTomonoService constructor.
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct($session)
    {
        $this->session = $session;
        $this->apiUrl = Yii::$app->params['taplinkApiUrl'];
        $this->_httpClient = new Client(['transport' => 'yii\httpclient\CurlTransport']);
    }


    /**
     * @param $profile_id
     * @param array $profiles
     * @param string $next
     * @param int $count
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getSubUsersData($profile_id, $next = '', &$profiles = [], $count = 4)
    {
        $this->authentication = $profile_id . '.' . $this->session;
        $headers = [
            'Content-Type' => 'application/json',
            'count' => $count,
            'next' => $next
        ];
        $response = $this->sendRequest([], self::TAPLINK_USER_PROFILES_URL, self::METHOT_GET, $headers);

        if (empty($response)) {
            return $profiles;
        }

        $profiles = array_merge_recursive($profiles, $response['response']['profiles']['fields']);
        $nextParam = $response['response']['profiles']['next'];
        if (!empty($nextParam)) {
            $this->getSubUsersData($profile_id, $nextParam, $profiles);
        }
        return $profiles;

    }

    /**
     * @param string $pageId
     * @param string $period
     * @param int $periodBack
     * period_back - previous period count
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getSubUserBatch($profile_id, $pageId, int $periodBack = 0, string $period = 'day')
    {
        $this->authentication = $profile_id . '.' . $this->session;
        $params = [
            "page_id" => $pageId,
            "period" => $period,
            "period_back" => $periodBack,
            "scope" => [
                "chart",
                "clicks"
            ],
            "_endpoints" => [
                "statistics/get",
                "pages/list"
            ]
        ];
        return $this->sendRequest($params, self::TAPLINK_BATCH_URL, self::METHOT_POST);
    }

    /**
     * @param string $email
     * @param string $password
     * @param null $token
     * @param string $twofactor
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function loginUser(string $email, string $password, $token = null, string $twofactor = '')
    {
        $params = [
            'email' => $email,
            'password' => $password,
            'token' => $token,
            'twofactor' => $twofactor
        ];
        return $this->sendRequest($params, self::TAPLINK_LOGIN_URL, self::METHOT_GET);
    }

    /**
     * @param array $params
     * @param string $url
     * @param string $method
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function sendRequest(array $params, string $url, string $method = self::METHOT_POST, $headers = ['Content-Type' => 'application/json'])
    {
        if (!empty($this->authentication)) {
            $headers = ArrayHelper::merge($headers, ['authentication' => 'Barier ' . $this->authentication]);
        }

        /**
         * @var Response $response
         */
        $response = $this->_httpClient->createRequest()
            ->setMethod($method)
            ->setFormat(Client::FORMAT_JSON)
            ->addHeaders($headers)
            ->setUrl($this->apiUrl . $url)
            ->setData($params)
            ->send();
        $response = json_decode($response->getContent(), true);
        if ((isset($response['result']) && $response['result'] === 'success') ||
            (isset($response['batch'][0]['result']) && $response['batch'][0]['result'] === 'success')) {
            SystemLogService::create(Logger::LEVEL_INFO,
                'Success',
                TaplinkClickStatistic::LOG_PREFIX, get_called_class());
            return $response;
        }
        SystemLogService::create(Logger::LEVEL_ERROR,
            json_encode($response),
            TaplinkClickStatistic::LOG_PREFIX, get_called_class());
        return false;

    }
}