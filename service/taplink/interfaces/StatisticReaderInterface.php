<?php

namespace app\service\taplink\interfaces;

interface StatisticReaderInterface
{
    public function getSubject(): string;
    public function getText(): string;
}
