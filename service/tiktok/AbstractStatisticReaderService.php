<?php

namespace app\service\tiktok;

use app\models\taplink\statistic\TaplinkClickStatistic;
use app\models\tiktok\statistic\TiktokStrimStatistic;
use app\service\systemLog\SystemLogService;
use DateInterval;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;
use yii\log\Logger;

abstract class AbstractStatisticReaderService
{
    public const TRANSPORT_ID = 1;
    public const METHOT_POST = 'post';
    public const METHOT_GET = 'get';
    public const TIKTOK_INSIGHS_URL = 'aweme/v1/data/insighs/';
    public const TIKTOK_LOGIN_URL = 'aweme/v1/data/insighs/';
    public $insigh_types;

    protected $_httpClient;
    protected $tiktokApiUrl;
    protected $user;
    protected $sessionId;
    protected $processedParams = [];


    /**
     * AbstractTomonoService constructor.
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct($sessionId)
    {
        $this->tiktokApiUrl = Yii::$app->params['tiktokApiUrl'];
        $this->sessionId = $sessionId;
        $this->_httpClient = new Client(['transport' => 'yii\httpclient\CurlTransport']);
    }

    /**
     * @param array $insigh_types
     * @param int $days
     * @param int $endDays
     * @return false|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getInvolvementDataByInsighTypes(array $insigh_types, $days = 2, $endDays = 0)
    {
        $this->insigh_types = $insigh_types;
        $types = [];
        foreach ($insigh_types as $insigh_type) {
            $types[] = [
                "insigh_type" => (string)$insigh_type,
                "days" => $days,
                "end_days" => $endDays,
            ];
        }
        $types[] = ['insigh_type' => 'user_info'];
        $data = $this->sendRequest([
            'type_requests' => \json_encode($types)],
            $this->tiktokApiUrl . self::TIKTOK_INSIGHS_URL,
            self::METHOT_POST, ['Content-Type' => 'multipart/form-data'],
            Client::FORMAT_CURL
        );
        if (count(array_intersect_key(array_flip($this->insigh_types), $data)) === count($this->insigh_types)) {
            SystemLogService::create(Logger::LEVEL_INFO,
                'Success',
                TiktokStrimStatistic::LOG_PREFIX, get_called_class());
            return $this->updateResponseParams($data, $days);
        }
        SystemLogService::create(Logger::LEVEL_ERROR,
            json_encode($data),
            TiktokStrimStatistic::LOG_PREFIX, get_called_class());
        return false;
    }


    public function loginUser(string $email, string $password)
    {
        $params = [
            'email' => $email,
            'password' => $password,
        ];
        return $this->sendRequest($params, self::TIKTOK_LOGIN_URL, self::METHOT_GET);
    }

    /**
     * @param array $params
     * @param string $url
     * @param string $method
     * @param string[] $headers
     * @return false|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function sendRequest(array $params, string $url, string $method = self::METHOT_POST, $headers = ['Content-Type' => 'application/json'], $format = Client::FORMAT_JSON)
    {
        if (!empty($this->sessionId)) {
            $headers = ArrayHelper::merge($headers, ['Cookie' => 'sessionid=' . $this->sessionId]);
        }
        $response = $this->_httpClient->createRequest()
            ->setMethod($method)
            ->setFormat($format)
            ->addHeaders($headers)
            ->setUrl($url)
            ->setData($params)
            ->send();
        return json_decode($response->getContent(), true);
    }


    /**
     * @param array $array
     * @param array $periodInDays
     * @return array
     */
    public function setProcessedParams(string $type, array $array, int $startDays): array
    {
        foreach ($array as $item) {
            $key = date('d.m.Y', strtotime('-' . $startDays-- . ' day'));
            if (isset($item['value'])) {
                $this->processedParams[$key][$type] = $item['value'];
            }
        }
        return  [];
    }

    /**
     * @param array $data
     * @param array $periodInDays
     * @return array
     */
    public function updateResponseParams(array $data, int $startDays): array
    {
        foreach ($this->insigh_types as $type) {
            if (isset($data[$type])) {
                $this->setProcessedParams($type, $data[$type], $startDays);
            }
        }
        return $this->processedParams;
    }

}