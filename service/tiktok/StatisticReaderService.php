<?php

namespace app\service\tiktok;

class StatisticReaderService extends AbstractStatisticReaderService
{

    /**
     * @param array $params
     * @return array|bool
     */
    public function getInvolvementStatisticData($insigh_types, $days, $endDays)
    {
        return $this->getInvolvementDataByInsighTypes($insigh_types, $days, $endDays);
    }
}