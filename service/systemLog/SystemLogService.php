<?php
namespace app\service\systemLog ;


use app\models\systemLog\SystemLog;
use Yii;
use yii\base\BaseObject;

class SystemLogService
{

    /**
     * @param string $message
     * @param string $prefix
     * @param string|null $category
     * @return bool
     */
    public static function create(string $logLevel, string $message, string $prefix = '', string $category = null): bool
    {
        $log = new SystemLog();
        $log->level = $logLevel;
        $log->message = $message;
        $log->category = (!empty($category)) ? $category : get_called_class();
        $log->prefix = $prefix;
        $log->log_time = Yii::$app->formatter->asTimestamp(date('Y-m-d H:i:s'));

        return $log->save();
    }

}