<?php

namespace app\service\binpartner;

class RegistrationReaderService extends AbstractStatisticReaderService
{
    private const METHOD = 'stats/registrations/parse';
    /**
     * @param array $params
     * @return array|bool
     */
    public function getRegistrationData(array $params)
    {
        return $this->getDataByParams($params,self::METHOD);

    }
}