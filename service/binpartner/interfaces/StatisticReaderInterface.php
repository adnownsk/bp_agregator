<?php

namespace app\service\binpartner\interfaces;

interface StatisticReaderInterface
{
    public function getSubject(): string;
    public function getText(): string;
}
