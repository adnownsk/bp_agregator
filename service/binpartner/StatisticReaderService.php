<?php

namespace app\service\binpartner;

class StatisticReaderService extends AbstractStatisticReaderService
{
    private const METHOD = 'stats/common/day/parse';

    /**
     * @param array $params
     * @return array|bool
     */
    public function getStatisticData(array $params)
    {
        return $this->getDataByParams($params, self::METHOD);

    }
}