<?php

namespace app\service\binpartner;

use app\models\binpartner\statistic\BinpartnerStatistic;
use app\service\systemLog\SystemLogService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;
use yii\log\Logger;

class AbstractStatisticReaderService
{
    public $params = [];
    public $data = [];
    public const TRANSPORT_ID = 1;
    protected $_httpClient;
    protected $url;
    protected $token;


    /**
     * AbstractTomonoService constructor.
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct()
    {
        $this->token = Yii::$app->params['statisticApiToken'];
        $this->url = Yii::$app->params['statisticApiUrl'];
        $this->_httpClient = new Client(['transport' => 'yii\httpclient\CurlTransport']);
    }


    /**
     * @param $params
     * @return array|bool
     */
    public function getDataByParams(array $params, $method, $page = 1, array &$data = [])
    {
        $params['page'] = $page;
        $response = $this->sendRequest($params, $method);
        if (isset($response['status']) && $response['status']!== 200) {
            SystemLogService::create(Logger::LEVEL_ERROR,
                json_encode($response),
                BinpartnerStatistic::LOG_PREFIX, get_called_class());
            return false;
        }

        if (empty($response)){
            return $data;
        }

        $data = array_merge($data, $response['table']);
        SystemLogService::create(Logger::LEVEL_INFO,
            'Success', BinpartnerStatistic::LOG_PREFIX
            , get_called_class());

        $pagination = $response['pagination'] ?? null;
        if (!empty($pagination) && ($pagination['current'] < $pagination['pages'])) {
            $this->getDataByParams($params, $method, $pagination['current'] + 1, $data);
        }

        return $data;
    }

    /**
     * @param $params
     * @return array|bool
     */
    public function getProfileParams($params, $method)
    {
        $response = $this->sendRequest($params, $method);
        if (isset($response['status']) && $response['status']!== 200) {
            SystemLogService::create(Logger::LEVEL_ERROR,
                json_encode($response),
                BinpartnerStatistic::LOG_PREFIX, get_called_class());
            return false;
        }

        SystemLogService::create(Logger::LEVEL_INFO,
            'Success', BinpartnerStatistic::LOG_PREFIX
            , get_called_class());

        return $response;
    }

    public function sendRequest(array $params, $method)
    {
        $headers = ['Content-Type' => 'application/json'];

        if (!empty($this->token)) {
            $headers = ArrayHelper::merge($headers, ['authorization' => 'Bearer ' . $this->token]);
        }

        /**
         * @var Response $response
         */
        $response = $this->_httpClient->createRequest()
            ->setMethod('post')
            ->setFormat(Client::FORMAT_JSON)
            ->addHeaders($headers)
            ->setUrl($this->url . $method)
            ->setData($params)
            ->send();
        return json_decode($response->getContent(), true);
    }
}