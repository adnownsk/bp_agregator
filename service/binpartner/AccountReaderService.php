<?php

namespace app\service\binpartner;

class AccountReaderService extends AbstractStatisticReaderService
{
    private const METHOD = 'profile/info';

    /**
     * @param array $params
     * @return array|bool
     */
    public function getAccountData(array $params)
    {
        return $this->getProfileParams($params, self::METHOD);

    }
}