<?php

namespace app\commands;

use app\models\binpartner\account\BinpartnerAccountParams;
use app\models\binpartner\registration\BinpartnerRegistration;
use app\models\binpartner\statistic\BinpartnerStatistic;
use app\service\binpartner\AccountReaderService;
use app\service\binpartner\RegistrationReaderService;
use app\service\binpartner\StatisticReaderService;
use yii\console\Controller;

class BinpartnerController extends Controller
{
    public function actionSetStatisticByDay()
    {
        $accounts = BinpartnerAccountParams::getAllActiveAccount();
        $statisticReadeService = new StatisticReaderService();
        $fromTime = strtotime("-100 day");
        foreach ($accounts as $account) {
            $params['user_token'] = $account->access_token;
            $params['period_from'] = date("Y-m-d", $fromTime);//"2022-02-25"
            $params['period_to'] = date('Y-m-d'); //"2022-02-25"
            $data = $statisticReadeService->getStatisticData($params);
            if (!empty($data)) {
                foreach ($data as &$statisticParam) {
                    $statisticParam['binpartner_account_params_id'] = $account->id;
                     BinpartnerStatistic::updateOrCreateStatistic($statisticParam);
                }
            }
        }
    }

    public function actionSetAccountBalance()
    {
        $accounts = BinpartnerAccountParams::getAllActiveAccount();
        $accountReadeService = new AccountReaderService();
        foreach ($accounts as $account) {
            $params['user_token'] = $account->access_token;
            $data = $accountReadeService->getAccountData($params);
            if (!empty($data) && ((float)$account->balance !== $data['balance_value'])) {
                $account->balance = $data['balance_value'];
                $account->save();
            }
        }
    }

    public function actionSetRegistration()
    {
        $fromTime = strtotime("-100 day");
        $accounts = BinpartnerAccountParams::getAllActiveAccount();
        $registrationReadeService = new RegistrationReaderService();
        foreach ($accounts as $account) {
            $params['user_token'] = $account->access_token;
            $params['period_from'] = date("Y-m-d", $fromTime);//"2022-02-25"
            $params['period_to'] = date('Y-m-d');
            $data = $registrationReadeService->getRegistrationData($params);
            if (!empty($data)) {
                foreach ($data as &$registrationParam) {
                    $registrationParam['binpartner_account_params_id'] = $account->id;
                    BinpartnerRegistration::updateOrCreateRegistration($registrationParam);
                }
            }
        }
    }


}