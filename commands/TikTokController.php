<?php

namespace app\commands;

use app\models\taplink\subAccount\TaplinkSubAccountParams;
use app\models\tiktok\account\TiktokAccountParams;
use app\models\tiktok\statistic\TiktokInvolvementStatistic;
use app\models\tiktok\statistic\TiktokStrimStatistic;
use app\service\tiktok\AbstractStatisticReaderService;
use app\service\tiktok\StatisticReaderService;
use Yii;
use yii\console\Controller;

class TikTokController extends Controller
{
    public const involvement_insigh_types = [
        'vv_history',
        'pv_history',
        'like_history',
        'comment_history',
        'share_history'
    ];

    public const strim_insigh_types = [
        'user_live_views_history',
        'user_live_unique_viewers_history',
        'user_live_diamonds_history',
        'user_live_gift_unique_viewers_history',
        'user_live_new_followers_history',
        'user_live_duration_time_history',
        'user_live_top_viewers_history',
        'self_rooms',
        'user_live_unique_viewers_28d',
        'user_live_unique_follower_viewers_28d',
        'user_live_gift_unique_viewers_28d',
        'user_live_follower_diamonds_history',
        'user_live_cnt_28d',
        'user_live_cnt_history',
        'user_info'];


    public function actionSetStrimStatisticBy28Days($days = 28, $endDays = 1)
    {
        $accounts = TiktokAccountParams::getAllActiveAccount();
        foreach ($accounts as $account) {
            $statisticReadeService = new StatisticReaderService($account->session_id);
            $data = $statisticReadeService->getInvolvementStatisticData(self::strim_insigh_types, $days, $endDays);
            if (!empty($data)) {
                foreach ($data as $date => $statistic) {
                    $statistic['tiktok_account_params_id'] = $account->id;
                    $statistic['date'] = $date;
                    TiktokStrimStatistic::updateOrCreateStatistic($statistic);
                }
            }
        }
    }

    public function actionSendEmailNotification()
    {
        $needUpdated = TiktokAccountParams::getNotUpdatedSessionIdAccountLinks();
        if (!empty($needUpdated)){
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['senderName'])
                ->setTo([Yii::$app->params['supportEmail1'],Yii::$app->params['supportEmail2']])
                ->setSubject('Обновите sessionid для аккаунтов Tiktok')
                ->setHtmlBody('Обновите sessionid для аккаунтов Tiktok в <a href="http://adminparser.m2corp.ru">adminparser.m2corp.ru</a>')
                ->send();
        }
    }

    public function actionSetInvolvementStatisticByDays($days = 28, $endDays = 2)
    {
        $accounts = TiktokAccountParams::getAllActiveAccount();
        foreach ($accounts as $account) {
            $statisticReadeService = new StatisticReaderService($account->session_id);
            $data = $statisticReadeService->getInvolvementStatisticData(self::involvement_insigh_types, $days, $endDays);
            if (!empty($data)) {
                foreach ($data as $date => $statistic) {
                    $statistic['tiktok_account_params_id'] = $account->id;
                    $statistic['date'] = $date;
                    TiktokInvolvementStatistic::updateOrCreateStatistic($statistic);
                }
            }
        }
    }

}