<?php

namespace app\commands;

use app\models\binpartner\account\BinpartnerAccountParams;
use app\models\binpartner\registration\BinpartnerRegistration;
use app\models\binpartner\statistic\BinpartnerStatistic;
use app\models\taplink\account\TaplinkAccountParams;
use app\models\taplink\statistic\TaplinkClickStatistic;
use app\models\taplink\subAccount\TaplinkSubAccountParams;
use app\service\binpartner\RegistrationReaderService;
use app\service\taplink\StatisticReaderService;
use app\service\taplink\SubUserReaderService;
use yii\console\Controller;

class TaplinkController extends Controller
{
    /**
     * @param int $daysAgo
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSetClickStatisticByDay($daysAgo = 0)
    {
        $subAccounts = TaplinkSubAccountParams::getAllActiveProAccount();
        foreach ($subAccounts as $subAccount) {
            $statisticReadeService = new StatisticReaderService($subAccount->taplinkAccountParams->session);
            for ($periodBack = 0; $periodBack <= (int)$daysAgo; $periodBack++) {
                $data = $statisticReadeService->getStatisticData($subAccount->profile_id, $subAccount->page_id,$periodBack);
                $statistic = $data['batch'][0]['response']['statistics'] ?? false;
                $date = $statistic['period']['title'] ?? false;
                if (!empty($data) && $statistic && $date) {
                    foreach ($statistic['clicks'] as &$statisticParam) {
                        $statisticParam['taplink_sub_account_params_id'] = $subAccount->id;
                        $statisticParam['date'] = $date;
                        TaplinkClickStatistic::updateOrCreateStatistic($statisticParam);
                    }
                }
            }
        }
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdateSubUsersAccounts()
    {
        $accounts = TaplinkAccountParams::getAllActiveAccount();
        foreach ($accounts as $account) {
            $subUserReadeService = new SubUserReaderService($account->session);
            $data = $subUserReadeService->getSubUsers($account->profile_id);
            if (!empty($data)) {
                foreach ($data as &$subAccountParams) {
                    TaplinkSubAccountParams::disableSubAccountsByAccountId($account->id);
                    $subAccountParams['taplink_account_params_id'] = $account->id;
                    TaplinkSubAccountParams::updateOrCreateSubAccount($subAccountParams);
                }
            }
        }
    }


}