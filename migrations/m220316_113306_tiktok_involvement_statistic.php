<?php

use yii\db\Migration;

/**
 * Class m220316_113306_tiktok_involvement_statistic
 */
class m220316_113306_tiktok_involvement_statistic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }
        $this->createTable('{{%tiktok_involvement_statistic}}', [
            'id' => $this->primaryKey(),
            'tiktok_account_params_id' =>$this->integer(),
            'date' => $this->integer(),
            'vv' => $this->integer()->null(),
            'pv' => $this->integer()->null(),
            'like' => $this->integer()->null(),
            'comment' => $this->integer()->null(),
            'share' => $this->integer()->null(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_tiktok_involvement_statistic_tiktok_account_params_id', 'tiktok_involvement_statistic', 'tiktok_account_params_id',
            'tiktok_account_params', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_tiktok_involvement_statistic_tiktok_account_params_id', 'tiktok_involvement_statistic');
        $this->dropTable('{{%tiktok_involvement_statistic}}');
    }
}
