<?php

use yii\db\Migration;

/**
 * Class m220317_102233_tiktok_strim_statistic
 */
class m220317_102233_tiktok_strim_statistic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }
        $this->createTable('{{%tiktok_strim_statistic}}', [
            'id' => $this->primaryKey(),
            'tiktok_account_params_id' => $this->integer(),
            'date' => $this->integer(),
            'live_views' => $this->integer()->null(),
            'live_unique_viewers' => $this->integer()->null(),
            'live_diamonds' => $this->integer()->null(),
            'live_gift_unique_viewers' => $this->integer()->null(),
            'live_new_followers' => $this->integer()->null(),
            'live_duration_time' => $this->integer()->null(),
            'live_top_viewers' => $this->integer()->null(),
            'live_follower_diamonds' => $this->integer()->null(),
            'live_cnt' => $this->integer()->null(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_tiktok_strim_statistic_tiktok_account_params_id', 'tiktok_strim_statistic', 'tiktok_account_params_id',
            'tiktok_account_params', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_tiktok_strim_statistic_tiktok_account_params_id', 'tiktok_strim_statistic');
        $this->dropTable('{{%tiktok_strim_statistic}}');
    }
}
