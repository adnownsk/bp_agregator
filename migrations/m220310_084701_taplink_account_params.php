<?php

use yii\db\Migration;

/**
 * Class m220310_084701_taplink_account_params
 */
class m220310_084701_taplink_account_params extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }

        $this->createTable('{{%taplink_account_params}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->unique()->notNull(),
            'password' => $this->string()->null(),
            'session' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(\app\models\taplink\account\TaplinkAccountParams::STATUS_ENABLE),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'plan_paid' => $this->string(),
            'tariff' => $this->string(),
            'tariff_ends' =>$this->boolean(),
            'tariff_ends_days' =>$this->integer(),
            'tariff_until' =>$this->integer(),
            'trial_tariff' =>$this->boolean()->null(),
            'trial_activated' =>$this->boolean()->defaultValue(false),
            'username' =>$this->string(),
            'domain' =>$this->string(),
            'domain_id' =>$this->integer(),
            'profile_id' =>$this->integer(),
            'user_id' =>$this->integer(),
            'account_id' =>$this->integer(),
            'session_account_id' =>$this->integer(),
            'hash' =>$this->string(),
            'country' =>$this->string(4),
            'favourites'=>$this->string(512),//profile_id,nickname,is_share
            'partner_id'=>$this->integer()->null(),
            'invite_partner_id'=>$this->integer()->null(),
            'has_nickname'=>$this->integer(),
            'tips_bits' => $this->integer(),
            'utc_timezone' => $this->smallInteger(),
            'nickname' => $this->string(),
            'access' => $this->integer(),
            'appsumo_user_id' => $this->integer()->null(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%taplink_account_params}}');
    }
}
