<?php

use yii\db\Migration;

/**
 * Class m220303_085647_binpartner_registration
 */
class m220303_085647_binpartner_registration extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }
        $this->createTable('{{%binpartner_registration}}', [
            'id' => $this->primaryKey(),
            'binpartner_account_params_id' =>$this->integer(),
            'trader_id' => $this->integer()->null(),
            'campaign'=> $this->string()->null(),
            'subaccount'=> $this->string()->null(),
            'registration_date' => $this->integer(),
            'country' => $this->string()->null(),
            'created_at' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_binpartner_registration_binpartner_account_params_id', 'binpartner_registration', 'binpartner_account_params_id',
            'binpartner_account_params', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_binpartner_registration_binpartner_account_params_id', 'binpartner_registration');
        $this->dropTable('{{%binpartner_registration}}');
    }
}
