<?php

use yii\db\Migration;

/**
 * Class m220316_113252_tiktok_account_params
 */
class m220316_113252_tiktok_account_params extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }

        $this->createTable('{{%tiktok_account_params}}', [
            'id' => $this->primaryKey(),
            'session_id' => $this->string(512)->null(),
            'username' => $this->string()->unique()->notNull(),
            'password' => $this->string()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(app\models\tiktok\account\TiktokAccountParams::STATUS_ENABLE),
            'expired_at' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%tiktok_account_params}}');
    }
}
