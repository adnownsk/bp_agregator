<?php

use yii\db\Migration;

/**
 * Class m220310_084808_taplink_sub_account_params
 */
class m220310_084808_taplink_sub_account_params extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }

        $this->createTable('{{%taplink_sub_account_params}}', [
            'id' => $this->primaryKey(),
            'taplink_account_params_id' =>$this->integer(),
            'is_completed'=> $this->smallInteger(),
            'page_id'=>$this->integer(),
            'profile_status_id'=>$this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(\app\models\taplink\subAccount\TaplinkSubAccountParams::STATUS_ENABLE),
            'tariff_current'=> $this->string(),
            'is_favourite'=>$this->integer(),
            'is_transfer_pending'=>$this->integer(),
            'profile_id'=>$this->integer(),
            'link'=>$this->string(),
            'tms_created'=>$this->integer(),
            'tariff_until'=>$this->integer()->null(),
            'tariff'=>$this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_taplink_sub_account_params_taplink_account_params_id', 'taplink_sub_account_params', 'taplink_account_params_id',
            'taplink_account_params', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_taplink_sub_account_params_taplink_account_params_id', 'taplink_sub_account_params');
        $this->dropTable('{{%taplink_sub_account_params}}');
    }
}
