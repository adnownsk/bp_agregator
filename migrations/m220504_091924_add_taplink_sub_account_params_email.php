<?php

use yii\db\Migration;

/**
 * Class m220504_091924_add_taplink_sub_account_params_email
 */
class m220504_091924_add_taplink_sub_account_params_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%taplink_sub_account_params}}','email', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%taplink_sub_account_params}}', 'email');
    }
}
