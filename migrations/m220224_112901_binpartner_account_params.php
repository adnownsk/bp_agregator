<?php

use yii\db\Migration;

/**
 * Class m220224_112901_binpartner_account_params
 */
class m220224_112901_binpartner_account_params extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }

        $this->createTable('{{%binpartner_account_params}}', [
            'id' => $this->primaryKey(),
            'access_token' => $this->string(512)->null(),
            'email' => $this->string()->unique()->notNull(),
            'password' => $this->string()->null(),
            'status' => $this->smallInteger()->notNull()->defaultValue(app\models\binpartner\account\BinpartnerAccountParams::STATUS_ENABLE),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%binpartner_account_params}}');
    }
}
