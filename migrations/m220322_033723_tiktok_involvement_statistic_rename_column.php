<?php

use yii\db\Migration;

/**
 * Class m220322_033723_tiktok_involvement_statistic_rename_column
 */
class m220322_033723_tiktok_involvement_statistic_rename_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%tiktok_involvement_statistic}}', 'like', 'like_history');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%tiktok_involvement_statistic}}', 'like_history', 'like');
    }

}
