<?php

use yii\db\Migration;

/**
 * Class m220228_050912_binpartnerStatistic
 */
class m220228_050912_binpartner_statistic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }
        $this->createTable('{{%binpartner_statistic}}', [
            'id' => $this->primaryKey(),
            'binpartner_account_params_id' =>$this->integer(),
            'day' => $this->integer(),
            'visitors' => $this->integer()->null(),
            'reg' => $this->integer()->null(),
            'conf' => $this->integer()->null(),
            'ctr' => $this->decimal(16,2),
            'demo' => $this->integer()->null(),
            'ftd' => $this->integer()->null(),
            'ftds' => $this->decimal(16,2),
            'first_ratio' => $this->decimal(16,2),
            'sign_up_ratio' => $this->decimal(16,2),
            'deposits_count' => $this->integer()->null(),
            'deposits' => $this->decimal(16,2),
            'traders' => $this->integer()->null(),
            'deals_count' => $this->integer()->null(),
            'deals' => $this->decimal(16,2),
            'flows' => $this->decimal(16,2),
            'withs' => $this->decimal(16,2),
            'profit' => $this->decimal(16,2),
            'created_at' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_binpartner_statistic_binpartner_account_params_id', 'binpartner_statistic', 'binpartner_account_params_id',
            'binpartner_account_params', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_binpartner_statistic_binpartner_account_params_id', 'binpartner_statistic');
        $this->dropTable('{{%binpartner_statistic}}');
    }
}
