<?php

use yii\db\Migration;

/**
 * Class m220408_093219_add_tiktok_account_params_email
 */
class m220408_093219_add_tiktok_account_params_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tiktok_account_params}}','email', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%tiktok_account_params}}', 'email');
    }
}
