<?php

use yii\db\Migration;

/**
 * Class m220310_084903_taplink_click_statistic
 */
class m220310_084903_taplink_click_statistic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ';
        }
        $this->createTable('{{%taplink_click_statistic}}', [
            'id' => $this->primaryKey(),
            'taplink_sub_account_params_id' =>$this->integer(),
            'date' => $this->integer(),
            'created_at' => $this->integer(),
            'block_id' => $this->integer(),
            'slot_id' => $this->integer(),
            'clicks' => $this->integer(),
            'cv' => $this->decimal(16,2),
            'type' => $this->string(),
            'title' => $this->string(),

        ], $tableOptions);
        $this->addForeignKey('fk_taplink_click_statistic_taplink_sub_account_params_id', 'taplink_click_statistic', 'taplink_sub_account_params_id',
            'taplink_sub_account_params', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_taplink_click_statistic_taplink_sub_account_params_id', 'taplink_click_statistic');
        $this->dropTable('{{%taplink_click_statistic}}');
    }
}
