<?php

use yii\db\Migration;

/**
 * Class m220601_040243_add_binpartner_account_params_balance
 */
class m220601_040243_add_binpartner_account_params_balance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%binpartner_account_params}}','balance', $this->decimal(16,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%binpartner_account_params}}', 'balance');
    }
}
