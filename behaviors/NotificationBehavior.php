<?php

namespace app\behaviors;

use app\models\tiktok\account\TiktokAccountParams;
use Yii;
use yii\base\Behavior;
use yii\base\Controller;

class NotificationBehavior extends Behavior
{
    public function events(): array
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'notification',
        ];
    }

    public function notification()
    {
        $needUpdated = TiktokAccountParams::getNotUpdatedSessionIdAccountLinks();
        if (!empty($needUpdated)){
            Yii::$app->session->setFlash('error', 'User needs to update session id: ' . $needUpdated);
        }
    }
}