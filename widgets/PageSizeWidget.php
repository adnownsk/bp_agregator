<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\data\Pagination;

class PageSizeWidget extends Widget
{
    public $model;
    public $sizes =  [
        25 => 25,
        50 => 50,
        100 => 100,
        250 => 250,
    ];

    public function init()
    {
        $pagination = new Pagination([
            'pageSize' => Yii::$app->request->get('pageSize', 25),
        ]);

        echo $this->render('' . 'page-size', [
            'current' => $this->model->pageSize ?? current($this->sizes),
            'sizes' => $this->sizes,
            'pagination' => $pagination,
        ]);
    }
}