<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'method' => 'GET',
    'action' => '',
    'id' => 'page_size_form'
]); ?>

<?= Html::dropDownList('pageSize', $pagination->pageSize,
    $sizes, [
        'onchange' => "$('#page_size_form').submit();",
        'style' => 'position: relative;float: right; padding: 6px 14px;',
    ])
?>

<?php ActiveForm::end(); ?>
