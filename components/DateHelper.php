<?php

namespace app\components;

use yii\db\ActiveQuery;

class DateHelper
{
    /**
     * @param ActiveQuery $query
     * @param string $field
     * @param string $dateRangeString
     */
    public static function addQueryByDateRange($query, $field, $dateRangeString)
    {
        if (empty($dateRangeString)) {
            return;
        }

        $exploded = explode(' - ', $dateRangeString);
        if (empty($exploded[0]) || empty($exploded[1])) {
            return;
        }

        list($dateFrom, $dateTo) = $exploded;

        $query->andFilterWhere([
            'BETWEEN',
            $field,
            strtotime(date('Y-m-d 00:00:00', strtotime($dateFrom))),
            strtotime(date('Y-m-d 23:59:59', strtotime($dateTo))),
        ]);
    }
}