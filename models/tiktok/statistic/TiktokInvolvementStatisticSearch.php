<?php

namespace app\models\tiktok\statistic;

use app\components\DateHelper;
use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\tiktok\statistic\TiktokInvolvementStatistic;

/**
 * TiktokStatisticSearch represents the model behind the search form of `app\models\tiktok\statistic\TiktokStatistic`.
 */
class TiktokInvolvementStatisticSearch extends TiktokInvolvementStatistic
{
    public $pageSize;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tiktok_account_params_id', 'pageSize', 'vv', 'pv', 'like_history', 'comment', 'share', 'created_at'], 'integer'],
            [['updated_at', 'date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TiktokInvolvementStatistic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->request->get('pageSize', 25),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiktok_account_params_id' => $this->tiktok_account_params_id,
            'vv' => $this->vv,
            'pv' => $this->pv,
            'like_history' => $this->like_history,
            'comment' => $this->comment,
            'share' => $this->share,
        ]);
        DateHelper::addQueryByDateRange($query, self::tableName() . '.updated_at', $this->updated_at);
        DateHelper::addQueryByDateRange($query, self::tableName() . '.date', $this->date);

        return $dataProvider;
    }
}
