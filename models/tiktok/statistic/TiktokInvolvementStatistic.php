<?php

namespace app\models\tiktok\statistic;

use app\models\BaseModel;
use app\models\tiktok\account\TiktokAccountParams;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tiktok_statistic".
 *
 * @property int $id
 * @property int|null $tiktok_account_params_id
 * @property int|null $date
 * @property int|null $vv
 * @property int|null $pv
 * @property int|null $like_history
 * @property int|null $comment
 * @property int|null $share
 * @property int|null $updated_at
 * @property int|null $created_at
 *
 * @property TiktokAccountParams $tiktokAccountParams
 */
class TiktokInvolvementStatistic extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tiktok_involvement_statistic';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tiktok_account_params_id', 'date', 'vv', 'pv', 'like_history', 'comment', 'share', 'updated_at', 'created_at'], 'integer'],
            [['tiktok_account_params_id'], 'exist', 'skipOnError' => true, 'targetClass' => TiktokAccountParams::className(), 'targetAttribute' => ['tiktok_account_params_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tiktok_account_params_id' => Yii::t('app', 'Tiktok Account Params ID'),
            'date' => Yii::t('app', 'Date'),
            'vv' => Yii::t('app', 'Vv'),
            'pv' => Yii::t('app', 'Pv'),
            'like_history' => Yii::t('app', 'Like'),
            'comment' => Yii::t('app', 'Comment'),
            'share' => Yii::t('app', 'Share'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public static function updateOrCreateStatistic($params)
    {
        $params = self::getValidParams($params);
        $attributes = [
            'tiktok_account_params_id' => $params['tiktok_account_params_id'],
            'date' => $params['date']
        ];
        return self::updateOrCreate($attributes, $params);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function getValidParams(array $data): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tiktok_account_params_id' => (int)$data['tiktok_account_params_id'],
            'date' => strtotime($data['date']),
            'vv' => $data['vv_history'] ?? null,
            'pv' => $data['pv_history'] ?? null,
            'like_history' => $data['like_history'] ?? null,
            'comment' => $data['comment_history'] ?? null,
            'share' => $data['share_history'] ?? null,
        ];
    }

    /**
     * Gets query for [[TiktokAccountParams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiktokAccountParams()
    {
        return $this->hasOne(TiktokAccountParams::className(), ['id' => 'tiktok_account_params_id']);
    }
}
