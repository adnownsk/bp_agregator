<?php

namespace app\models\tiktok\statistic;

use app\models\BaseModel;
use app\models\tiktok\account\TiktokAccountParams;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tiktok_strim_statistic".
 *
 * @property int $id
 * @property int|null $tiktok_account_params_id
 * @property int|null $date
 * @property int|null $live_views
 * @property int|null $live_unique_viewers
 * @property int|null $live_diamonds
 * @property int|null $live_gift_unique_viewers
 * @property int|null $live_new_followers
 * @property int|null $live_duration_time
 * @property int|null $live_top_viewers
 * @property int|null $live_follower_diamonds
 * @property int|null $live_cnt
 * @property int|null $updated_at
 * @property int|null $created_at
 *
 * @property TiktokAccountParams $tiktokAccountParams
 */
class TiktokStrimStatistic extends BaseModel
{
    public const LOG_PREFIX = 'tik-tok_statistic_Reader';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tiktok_strim_statistic';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tiktok_account_params_id', 'date', 'live_views', 'live_unique_viewers', 'live_diamonds', 'live_gift_unique_viewers', 'live_new_followers', 'live_duration_time', 'live_top_viewers', 'live_follower_diamonds', 'live_cnt', 'updated_at', 'created_at'], 'integer'],
            [['tiktok_account_params_id'], 'exist', 'skipOnError' => true, 'targetClass' => TiktokAccountParams::className(), 'targetAttribute' => ['tiktok_account_params_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tiktok_account_params_id' => Yii::t('app', 'Tiktok Account Params ID'),
            'date' => Yii::t('app', 'Date'),
            'live_views' => Yii::t('app', 'User Live Views'),
            'live_unique_viewers' => Yii::t('app', 'User Live Unique Viewers'),
            'live_diamonds' => Yii::t('app', 'User Live Diamonds'),
            'live_gift_unique_viewers' => Yii::t('app', 'User Live Gift Unique Viewers'),
            'live_new_followers' => Yii::t('app', 'User Live New Followers'),
            'live_duration_time' => Yii::t('app', 'User Live Duration Time'),
            'live_top_viewers' => Yii::t('app', 'User Live Top Viewers'),
            'live_follower_diamonds' => Yii::t('app', 'User Live Follower Diamonds'),
            'live_cnt' => Yii::t('app', 'User Live Cnt'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public static function updateOrCreateStatistic($params)
    {
        $params = self::getValidParams($params);
        $attributes = [
            'tiktok_account_params_id' => $params['tiktok_account_params_id'],
            'date' => $params['date']
        ];
        return self::updateOrCreate($attributes, $params);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function getValidParams(array $data): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tiktok_account_params_id' => (int)$data['tiktok_account_params_id'],
            'date' => strtotime($data['date']),
            'live_views' => $data['user_live_views_history'] ?? null,
            'live_unique_viewers' => $data['user_live_unique_viewers_history'] ?? null,
            'live_diamonds' => $data['user_live_diamonds_history'] ?? null,
            'live_gift_unique_viewers' => $data['user_live_gift_unique_viewers_history'] ?? null,
            'live_new_followers' => $data['user_live_new_followers_history'] ?? null,
            'live_duration_time' => $data['user_live_duration_time_history'],
            'live_top_viewers' => $data['user_live_top_viewers_history'] ?? null,
            'live_follower_diamonds' => $data['user_live_follower_diamonds_history'] ?? null,
            'live_cnt' => $data['user_live_cnt_history'] ?? null,
        ];
    }


    /**
     * Gets query for [[TiktokAccountParams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiktokAccountParams()
    {
        return $this->hasOne(TiktokAccountParams::className(), ['id' => 'tiktok_account_params_id']);
    }
}
