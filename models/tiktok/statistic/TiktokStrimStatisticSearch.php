<?php

namespace app\models\tiktok\statistic;

use app\components\DateHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\tiktok\statistic\TiktokStrimStatistic;

/**
 * TiktokStrimStatisticSearch represents the model behind the search form of `app\models\tiktok\statistic\TiktokStrimStatistic`.
 */
class TiktokStrimStatisticSearch extends TiktokStrimStatistic
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tiktok_account_params_id', 'live_views', 'live_unique_viewers', 'live_diamonds', 'live_gift_unique_viewers', 'live_new_followers', 'live_top_viewers', 'live_follower_diamonds', 'live_cnt', 'created_at'], 'integer'],
            [['updated_at', 'date', 'live_duration_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TiktokStrimStatistic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiktok_account_params_id' => $this->tiktok_account_params_id,
            'live_views' => $this->live_views,
            'live_unique_viewers' => $this->live_unique_viewers,
            'live_diamonds' => $this->live_diamonds,
            'live_gift_unique_viewers' => $this->live_gift_unique_viewers,
            'live_new_followers' => $this->live_new_followers,
            'live_top_viewers' => $this->live_top_viewers,
            'live_follower_diamonds' => $this->live_follower_diamonds,
            'live_cnt' => $this->live_cnt,
            'created_at' => $this->created_at,
        ]);

        DateHelper::addQueryByDateRange($query, self::tableName() . '.updated_at', $this->updated_at);
        DateHelper::addQueryByDateRange($query, self::tableName() . '.live_duration_time', $this->live_duration_time);
        DateHelper::addQueryByDateRange($query, self::tableName() . '.date', $this->date);


        return $dataProvider;
    }
}
