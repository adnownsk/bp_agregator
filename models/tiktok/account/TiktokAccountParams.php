<?php

namespace app\models\tiktok\account;

use app\models\tiktok\statistic\TiktokInvolvementStatistic;
use app\service\tiktok\StatisticReaderService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "tiktok_account_params".
 *
 * @property int $id
 * @property string|null $session_id
 * @property string $username
 * @property string $email
 * @property string|null $password
 * @property int $status
 * @property int|null $created_at
 * @property int|null $expired_at
 * @property int|null $updated_at
 *
 * @property TiktokInvolvementStatistic[] $tiktokStatistics
 */
class TiktokAccountParams extends \yii\db\ActiveRecord
{
    const STATUS_DISABLE = 0;
    const STATUS_ENABLE = 1;
    public $serverResponse = [];


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tiktok_account_params';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username','email'], 'required'],
            [['status', 'created_at', 'updated_at', 'expired_at'], 'integer'],
            [['session_id'], 'string', 'max' => 512],
            [['username', 'password'], 'string', 'max' => 255],
            [['username','email'], 'unique'],
            ['session_id', 'validateSessionId'],
        ];
    }

    /**
     * @param $attribute
     * @throws \yii\base\InvalidConfigException
     */
    public function validateSessionId($attribute)
    {
        if ($this->status != self::STATUS_DISABLE) {
            $data = (new StatisticReaderService($this->session_id))->getInvolvementStatisticData(['user_info'], 6, 6);
            if ($data === false) {
                $this->addError($attribute, 'You entered an invalid session id.');
            }
        }
    }

    public function beforeSave($insert)
    {
        if ($this->getOldAttribute('session_id') !== $this->session_id) {
            $this->expired_at = strtotime("+2 months");
        }
        return parent::beforeSave($insert); //
    }

    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->session->setFlash('error', null);
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'session_id' => Yii::t('app', 'Session ID'),
            'username' => Yii::t('app', 'username'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'status' => Yii::t('app', 'Status'),
            'expired_at' => Yii::t('app', 'Expired At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_ENABLE => Yii::t('app', 'enable'),
            self::STATUS_DISABLE => Yii::t('app', 'disable'),
        ];
    }

    public static function getNotUpdatedSessionIdAccount()
    {
        return ArrayHelper::map(
            self::find()->where(['and',
                ['status' => self::STATUS_ENABLE],
                ['<=', 'expired_at', time()]])->all(),
            'id', 'username');
    }

    public static function getNotUpdatedSessionIdAccountLinks()
    {
        $links = '';
        $accounts = self::getNotUpdatedSessionIdAccount();
        if (!empty($accounts)) {
            foreach ($accounts as $id => $name) {
                $links .= Html::a($name, Url::to(['/tiktok-account/update', 'id' => $id])) . '  ';
            }
        }
        return $links;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllActiveAccount()
    {
        return self::find()->where(['status' => self::STATUS_ENABLE])->all();
    }

    /**
     * @return array
     */
    public static function getActiveAccountList(): array
    {
        return ArrayHelper::map(self::findall(['status' => self::STATUS_ENABLE]), 'id', 'username');
    }

    /**
     * Gets query for [[TiktokStatistics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiktokStatistics()
    {
        return $this->hasMany(TiktokInvolvementStatistic::className(), ['tiktok_account_params_id' => 'id']);
    }
}
