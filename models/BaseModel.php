<?php

namespace app\models;


abstract class BaseModel extends \yii\db\ActiveRecord
{
    /**
     * @param array $attributes
     * @param array $values
     * @return bool
     */
    public static function updateOrCreate(array $attributes, array $values = array())
    {
        $instance = static::firstOrNew($attributes);

        $instance->setAttributes($values);

        if (!$instance->save()){
            return false;
        }
        return true;
    }

    /**
     * @param array $attributes
     * @return static
     */
    public static function firstOrNew(array $attributes)
    {
        if (!is_null($instance = self::findOne($attributes))) {
            return $instance;
        }
        return new static($attributes);
    }

}
