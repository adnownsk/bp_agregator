<?php

namespace app\models\systemLog;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "system_log".
 *
 * @property integer $id
 * @property integer $level
 * @property string $category
 * @property integer $log_time
 * @property string $prefix
 * @property integer $message
 */
class SystemLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%system_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'log_time'], 'integer'],
            [['log_time'], 'required'],
            [['prefix', 'message'], 'string'],
            [['category'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'level' => Yii::t('app', 'Level'),
            'category' => Yii::t('app', 'Category'),
            'log_time' => Yii::t('app', 'Log Time'),
            'prefix' => Yii::t('app', 'Prefix'),
            'message' => Yii::t('app', 'Message'),
        ];
    }

    /**
     * @return array
     */
    public static function getPrefix(): array
    {
        $model = self::find()->all();

        return ArrayHelper::map($model, 'prefix', 'prefix');
    }
    public static function getMessageByPrefixAndType($prefix, $level)
    {
        return ArrayHelper::map(self::find()->select(['id','message'])->where(['prefix'=>$prefix,'level'=>$level])->all(),'id','message');
    }
}
