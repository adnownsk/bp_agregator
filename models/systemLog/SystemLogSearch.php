<?php

namespace app\models\systemLog;

use app\components\DateHelper;
use app\models\systemLog\SystemLog;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SystemLogSearch represents the model behind the search form about `app\models\SystemLog`.
 */
class SystemLogSearch extends SystemLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'message'], 'integer'],
            [['category', 'prefix', 'log_time', 'level'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'level' => $this->level,
            'message' => $this->message,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category]);
        if (is_array($this->prefix)){
            $query->andFilterWhere(['in', 'prefix', $this->prefix]);
        }else{
            $query->andFilterWhere(['like', 'prefix', $this->prefix]);
        }
        DateHelper::addQueryByDateRange($query, 'system_log.log_time', $this->log_time);

        return $dataProvider;
    }
}
