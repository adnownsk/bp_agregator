<?php

namespace app\models\taplink\subAccount;

use app\models\BaseModel;
use app\models\taplink\account\TaplinkAccountParams;
use app\models\taplink\statistic\TaplinkClickStatistic;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "taplink_sub_account_params".
 *
 * @property int $id
 * @property int|null $taplink_account_params_id
 * @property int|null $is_completed
 * @property int|null $page_id
 * @property int|null $profile_status_id
 * @property string|null $tariff_current
 * @property int|null $is_favourite
 * @property int $status
 * @property int|null $is_transfer_pending
 * @property int|null $profile_id
 * @property string|null $link
 * @property int|null $tms_created
 * @property int|null $tariff_until
 * @property string|null $tariff
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $access_token
 * @property string|null $password
 * @property string $email
 *
 * @property TaplinkAccountParams $taplinkAccountParams
 * @property TaplinkClickStatistic[] $taplinkClickStatistics
 */
class TaplinkSubAccountParams extends BaseModel
{
    const STATUS_DISABLE = 0;
    const STATUS_ENABLE = 1;
    const PRO_TARIFF = 'pro';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'taplink_sub_account_params';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taplink_account_params_id', 'is_completed', 'page_id', 'status', 'profile_status_id', 'is_favourite', 'is_transfer_pending', 'profile_id', 'tms_created', 'tariff_until', 'created_at', 'updated_at'], 'integer'],
            [['tariff_current', 'link', 'tariff'], 'string', 'max' => 255],
            [['link','email'], 'required'],
            ['email', 'email'],
            [['taplink_account_params_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaplinkAccountParams::className(), 'targetAttribute' => ['taplink_account_params_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'taplink_account_params_id' => Yii::t('app', 'Taplink Account Params ID'),
            'is_completed' => Yii::t('app', 'Is Completed'),
            'page_id' => Yii::t('app', 'Page ID'),
            'profile_status_id' => Yii::t('app', 'Profile Status ID'),
            'tariff_current' => Yii::t('app', 'Tariff Current'),
            'is_favourite' => Yii::t('app', 'Is Favourite'),
            'status' => Yii::t('app', 'Status'),
            'is_transfer_pending' => Yii::t('app', 'Is Transfer Pending'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'link' => Yii::t('app', 'Link'),
            'email' => Yii::t('app', 'Email'),
            'tms_created' => Yii::t('app', 'Tms Created'),
            'tariff_until' => Yii::t('app', 'Tariff Until'),
            'tariff' => Yii::t('app', 'Tariff'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[TaplinkAccountParams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaplinkAccountParams()
    {
        return $this->hasOne(TaplinkAccountParams::className(), ['id' => 'taplink_account_params_id']);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllActiveProAccount()
    {
        return self::find()->where(['status' => self::STATUS_ENABLE, 'tariff' => self::PRO_TARIFF])->all();
    }

    /**
     * @return array
     */
    public static function getActiveProAccountList(): array
    {
        return ArrayHelper::map(self::findall(['status' => self::STATUS_ENABLE, 'tariff' => self::PRO_TARIFF]),'id','link');
    }

    /**
     * @param $accountId
     */
    public static function disableSubAccountsByAccountId($accountId)
    {
        self::updateAll(['taplink_account_params_id' => $accountId], ['status' => self::STATUS_DISABLE]);
    }

    public static function updateOrCreateSubAccount($params)
    {
        $params = self::getValidParams($params);
        $attributes = [
            'taplink_account_params_id' => $params['taplink_account_params_id'],
            'profile_id' => $params['profile_id']
        ];
        $params['status'] = self::STATUS_ENABLE;
        return self::updateOrCreate($attributes, $params);
    }


    /**
     * Gets query for [[TaplinkClickStatistics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaplinkClickStatistics()
    {
        return $this->hasMany(TaplinkClickStatistic::className(), ['taplink_sub_account_params_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_ENABLE => Yii::t('app', 'enable'),
            self::STATUS_DISABLE => Yii::t('app', 'disable'),
        ];
    }

    /**
     * @param $param
     * @return array
     */
    public static function getParamList($param): array
    {
        return ArrayHelper::map(self::find()->select([$param])->all(), $param, $param);
    }

    /**
     * @param int $binPartnerAccountId
     * @param array $data
     * @return bool
     */
    public static function getValidParams(array $data): array
    {
        return [
            'taplink_account_params_id' => (int)$data['taplink_account_params_id'],
            'is_completed' => isset($data['is_completed']) ? (int)$data['is_completed'] : null,
            'page_id' => isset($data['page_id']) ? (int)$data['page_id'] : null,
            'profile_status_id' => isset($data['profile_status_id']) ? (int)$data['profile_status_id'] : null,
            'tariff_current' => $data['tariff_current'] ?? '',
            'is_favourite' => isset($data['is_favourite']) ? (int)$data['is_favourite'] : null,
            'is_transfer_pending' => isset($data['is_transfer_pending']) ? (int)$data['is_transfer_pending'] : null,
            'profile_id' => isset($data['profile_id']) ? (int)$data['profile_id'] : null,
            'link' => $data['link'] ?? '',
            'tms_created' => isset($data['tms_created']) ? strtotime($data['tms_created']) : null,
            'tariff_until' => isset($data['tariff_until']) ? strtotime($data['tariff_until']) : null,
            'tariff' => $data['tariff'] ?? '',
        ];
    }
}
