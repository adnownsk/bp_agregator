<?php

namespace app\models\taplink\subAccount;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\taplink\subAccount\TaplinkSubAccountParams;

/**
 * TaplinkSubAccountSearch represents the model behind the search form of `app\models\taplink\subAccount\TaplinkSubAccountParams`.
 */
class TaplinkSubAccountSearch extends TaplinkSubAccountParams
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'taplink_account_params_id', 'is_completed', 'page_id', 'profile_status_id', 'is_favourite', 'is_transfer_pending', 'profile_id', 'tms_created', 'tariff_until', 'created_at', 'updated_at'], 'integer'],
            [['tariff_current', 'email', 'link', 'tariff','status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaplinkSubAccountParams::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'taplink_account_params_id' => $this->taplink_account_params_id,
            'is_completed' => $this->is_completed,
            'page_id' => $this->page_id,
            'profile_status_id' => $this->profile_status_id,
            'is_favourite' => $this->is_favourite,
            'is_transfer_pending' => $this->is_transfer_pending,
            'profile_id' => $this->profile_id,
            'tms_created' => $this->tms_created,
            'tariff_until' => $this->tariff_until,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tariff_current', $this->tariff_current])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'tariff', $this->tariff])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['status', 'status', $this->status]);

        return $dataProvider;
    }
}
