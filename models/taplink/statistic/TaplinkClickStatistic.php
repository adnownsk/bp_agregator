<?php

namespace app\models\taplink\statistic;

use app\models\BaseModel;
use app\models\taplink\subAccount\TaplinkSubAccountParams;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "taplink_click_statistic".
 *
 * @property int $id
 * @property int|null $taplink_sub_account_params_id
 * @property int|null $date
 * @property int|null $created_at
 * @property int|null $block_id
 * @property int|null $slot_id
 * @property int|null $clicks
 * @property float|null $cv
 * @property string|null $type
 * @property string|null $title
 *
 * @property TaplinkSubAccountParams $taplinkSubAccountParams
 */
class TaplinkClickStatistic extends BaseModel
{
    const STATUS_DISABLE = 0;
    const STATUS_ENABLE = 1;
    public const LOG_PREFIX = 'taplink_statistic_Reader';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'taplink_click_statistic';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taplink_sub_account_params_id', 'date', 'created_at', 'block_id', 'slot_id', 'clicks'], 'integer'],
            [['cv'], 'number'],
            [['type', 'title'], 'string', 'max' => 255],
            [['taplink_sub_account_params_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaplinkSubAccountParams::className(), 'targetAttribute' => ['taplink_sub_account_params_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'taplink_sub_account_params_id' => Yii::t('app', 'Taplink Sub Account Params ID'),
            'date' => Yii::t('app', 'Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'block_id' => Yii::t('app', 'Block ID'),
            'slot_id' => Yii::t('app', 'Slot ID'),
            'clicks' => Yii::t('app', 'Clicks'),
            'cv' => Yii::t('app', 'Cv'),
            'type' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    public static function updateOrCreateStatistic($params)
    {
        $params = self::getValidParams($params);
        $attributes = [
            'taplink_sub_account_params_id' => $params['taplink_sub_account_params_id'],
            'date' => $params['date'],
            'title' => $params['title']
        ];
        return self::updateOrCreate($attributes, $params);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function getValidParams(array $data): array
    {
        return [
            'taplink_sub_account_params_id' => (int)$data['taplink_sub_account_params_id'],
            'date' => strtotime($data['date']),
            'block_id' => $data['block_id'] ?? null,
            'slot_id' => $data['slot_id']??'',
            'clicks' => $data['clicks'] ?? null,
            'cv' => isset($data['cv']) ? (float)$data['cv'] : null,
            'type' => $data['type'] ??'',
            'title' => $data['title'] ??'',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_ENABLE => Yii::t('app', 'enable'),
            self::STATUS_DISABLE => Yii::t('app', 'disable'),
        ];
    }

    /**
     * Gets query for [[TaplinkSubAccountParams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaplinkSubAccountParams()
    {
        return $this->hasOne(TaplinkSubAccountParams::className(), ['id' => 'taplink_sub_account_params_id']);
    }
}
