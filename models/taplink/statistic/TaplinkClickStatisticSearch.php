<?php

namespace app\models\taplink\statistic;

use app\components\DateHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\taplink\statistic\TaplinkClickStatistic;

/**
 * TaplinkClickStatisticSearch represents the model behind the search form of `app\models\taplink\statistic\TaplinkClickStatistic`.
 */
class TaplinkClickStatisticSearch extends TaplinkClickStatistic
{
    public $pageSize;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'pageSize', 'taplink_sub_account_params_id',  'created_at', 'block_id', 'slot_id', 'clicks'], 'integer'],
            [['cv'], 'number'],
            [['type', 'title', 'date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaplinkClickStatistic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->request->get('pageSize', 25),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'taplink_sub_account_params_id' => $this->taplink_sub_account_params_id,
            'created_at' => $this->created_at,
            'block_id' => $this->block_id,
            'slot_id' => $this->slot_id,
            'clicks' => $this->clicks,
            'cv' => $this->cv,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'title', $this->title]);
        DateHelper::addQueryByDateRange($query, self::tableName() . '.date', $this->date);

        return $dataProvider;
    }
}
