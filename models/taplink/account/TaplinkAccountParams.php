<?php

namespace app\models\taplink\account;

use app\models\taplink\subAccount\TaplinkSubAccountParams;
use app\service\taplink;
use app\service\taplink\AbstractStatisticReaderService;
use app\service\taplink\SubUserReaderService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "taplink_account_params".
 *
 * @property int $id
 * @property string $email
 * @property string|null $password
 * @property string|null $session
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $plan_paid
 * @property string|null $tariff
 * @property int|null $tariff_ends
 * @property int|null $tariff_ends_days
 * @property int|null $tariff_until
 * @property int|null $trial_tariff
 * @property int|null $trial_activated
 * @property string|null $username
 * @property string|null $domain
 * @property int|null $domain_id
 * @property int|null $profile_id
 * @property int|null $user_id
 * @property int|null $account_id
 * @property int|null $session_account_id
 * @property string|null $hash
 * @property string|null $country
 * @property string|null $favourites
 * @property int|null $partner_id
 * @property int|null $invite_partner_id
 * @property int|null $has_nickname
 * @property int|null $tips_bits
 * @property int|null $utc_timezone
 * @property string|null $nickname
 * @property int|null $access
 * @property int|null $appsumo_user_id
 */
class TaplinkAccountParams extends \yii\db\ActiveRecord
{
    const STATUS_DISABLE = 0;
    const STATUS_ENABLE = 1;
    public $serverResponse = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'taplink_account_params';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['status', 'created_at', 'updated_at', 'tariff_ends_days', 'tariff_until', 'trial_tariff', 'domain_id', 'profile_id', 'user_id', 'account_id', 'session_account_id', 'partner_id', 'invite_partner_id', 'has_nickname', 'tips_bits', 'utc_timezone', 'access', 'appsumo_user_id'], 'integer'],
            [['favourites', 'tariff_ends', 'trial_activated'], 'safe'],
            [['email', 'password', 'session', 'plan_paid', 'tariff', 'username', 'domain', 'hash', 'nickname'], 'string', 'max' => 255],
            [['country'], 'string', 'max' => 4],
            [['email'], 'unique'],
            [['password'], 'validateEmailPassword'],
        ];
    }


    public function afterSave($insert, $changedAttributes)
    {
        $subUserReadeService = new SubUserReaderService($this->session);
        $data = $subUserReadeService->getSubUsers($this->profile_id);
        if (!empty($data)) {
            foreach ($data as $subAccountParams) {
                TaplinkSubAccountParams::disableSubAccountsByAccountId($this->id);
                $subAccountParams['taplink_account_params_id'] = $this->id;
                TaplinkSubAccountParams::updateOrCreateSubAccount($subAccountParams);
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function validateEmailPassword($attribute, $params)
    {
        $data = !empty($this->serverResponse) ? $this->serverResponse : (new taplink\StatisticReaderService(''))->loginUser($this->email, $this->password);
        if ($data === false) {
            $this->addError($attribute, 'You entered an invalid login or password.');
        } else {
            $this->serverResponse = $data;
        }
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllActiveAccount()
    {
        return self::find()->where(['status' => self::STATUS_ENABLE])->all();
    }

    /**
     * @param int $binPartnerAccountId
     * @param array $data
     * @return bool
     */
    public static function getValidParams(array $data): array
    {
        $account = $data['account'];
        return [
            'session' => $data['session'],
            'plan_paid' => $account['plan_paid'] ?? '',
            'tariff' => $account['tariff'] ?? '',
            'tariff_ends' => $account['tariff_ends'] ?? null,
            'tariff_ends_days' => $account['tariff_ends_days'] ?? null,
            'tariff_until' => isset($account['tariff_until']) ? strtotime($account['tariff_until']) : null,
            'trial_tariff' => $account['trial_tariff'] ?? null,
            'trial_activated' => $account['trial_activated'] ?? null,
            'username' => $account['nickname'] ?? '',
            'domain' => $account['link_details']['domain'] ?? '',
            'domain_id' => $account['link_details']['domain_id'] ?? null,
            'profile_id' => $account['profile_id'] ?? null,
            'user_id' => $account['user']['user_id'] ?? null,
            'account_id' => $account['user']['account_id'] ?? null,
            'session_account_id' => $account['user']['session_account_id'] ?? null,
            'hash' => $account['user']['hash'] ?? '',
            'country' => $account['client']['country'] ?? '',
            'favourites' => json_encode($account['favourites'] ?? []),
            'partner_id' => $account['partner_id'] ?? null,
            'invite_partner_id' => $account['invite_partner_id'] ?? null,
            'has_nickname' => $account['has_nickname'] ?? null,
            'tips_bits' => $account['tips_bits'] ?? null,
            'utc_timezone' => $account['utc_timezone'] ?? 0,
            'nickname' => $account['nickname'] ?? '',
            'access' => $account['access'] ?? null,
            'appsumo_user_id' => $account['appsumo_user_id'] ?? null,
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'session' => Yii::t('app', 'Session'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'plan_paid' => Yii::t('app', 'Plan Paid'),
            'tariff' => Yii::t('app', 'Tariff'),
            'tariff_ends' => Yii::t('app', 'Tariff Ends'),
            'tariff_ends_days' => Yii::t('app', 'Tariff Ends Days'),
            'tariff_until' => Yii::t('app', 'Tariff Until'),
            'trial_tariff' => Yii::t('app', 'Trial Tariff'),
            'trial_activated' => Yii::t('app', 'Trial Activated'),
            'username' => Yii::t('app', 'Username'),
            'domain' => Yii::t('app', 'Domain'),
            'domain_id' => Yii::t('app', 'Domain ID'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'session_account_id' => Yii::t('app', 'Session Account ID'),
            'hash' => Yii::t('app', 'Hash'),
            'country' => Yii::t('app', 'Country'),
            'favourites' => Yii::t('app', 'Favourites'),
            'partner_id' => Yii::t('app', 'Partner ID'),
            'invite_partner_id' => Yii::t('app', 'Invite Partner ID'),
            'has_nickname' => Yii::t('app', 'Has Nickname'),
            'tips_bits' => Yii::t('app', 'Tips Bits'),
            'utc_timezone' => Yii::t('app', 'Utc Timezone'),
            'nickname' => Yii::t('app', 'Nickname'),
            'access' => Yii::t('app', 'Access'),
            'appsumo_user_id' => Yii::t('app', 'Appsumo User ID'),
        ];
    }

    /**
     * @return array
     */
    public static function getActiveAccountList(): array
    {
        return ArrayHelper::map(self::findall(['status' => self::STATUS_ENABLE]),'id','email');
    }

    /**
     * @param $param
     * @return array
     */
    public static function getParamList($param): array
    {
        return ArrayHelper::map(self::find()->select([$param])->all(), $param, $param);
    }

    /**
     * @return array
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_ENABLE => Yii::t('app', 'enable'),
            self::STATUS_DISABLE => Yii::t('app', 'disable'),
        ];
    }
}
