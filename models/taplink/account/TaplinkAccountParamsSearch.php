<?php

namespace app\models\taplink\account;

use app\components\DateHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\taplink\account\TaplinkAccountParams;

/**
 * TaplinkAccountParamsSearch represents the model behind the search form of `app\models\taplink\account\TaplinkAccountParams`.
 */
class TaplinkAccountParamsSearch extends TaplinkAccountParams
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'tariff_ends', 'tariff_ends_days', 'tariff_until', 'trial_tariff', 'trial_activated', 'domain_id', 'profile_id', 'user_id', 'account_id', 'session_account_id', 'partner_id', 'invite_partner_id', 'has_nickname', 'tips_bits', 'utc_timezone', 'access', 'appsumo_user_id'], 'integer'],
            [['email', 'password', 'session', 'plan_paid', 'updated_at', 'tariff', 'username', 'domain', 'hash', 'country', 'favourites', 'nickname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaplinkAccountParams::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'tariff_ends' => $this->tariff_ends,
            'tariff_ends_days' => $this->tariff_ends_days,
            'tariff_until' => $this->tariff_until,
            'trial_tariff' => $this->trial_tariff,
            'trial_activated' => $this->trial_activated,
            'domain_id' => $this->domain_id,
            'profile_id' => $this->profile_id,
            'user_id' => $this->user_id,
            'account_id' => $this->account_id,
            'session_account_id' => $this->session_account_id,
            'partner_id' => $this->partner_id,
            'invite_partner_id' => $this->invite_partner_id,
            'has_nickname' => $this->has_nickname,
            'tips_bits' => $this->tips_bits,
            'utc_timezone' => $this->utc_timezone,
            'access' => $this->access,
            'appsumo_user_id' => $this->appsumo_user_id,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'session', $this->session])
            ->andFilterWhere(['like', 'plan_paid', $this->plan_paid])
            ->andFilterWhere(['like', 'tariff', $this->tariff])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'domain', $this->domain])
            ->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'favourites', $this->favourites])
            ->andFilterWhere(['like', 'nickname', $this->nickname]);
        DateHelper::addQueryByDateRange($query, self::tableName() . '.updated_at', $this->updated_at);
        return $dataProvider;
    }
}
