<?php

namespace app\models\binpartner\account;

use app\components\DateHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BinpartnerAccountParamsSearch represents the model behind the search form of `app\models\binpartner\account\BinpartnerAccountParams`.
 */
class BinpartnerAccountParamsSearch extends BinpartnerAccountParams
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['updated_at','created_at'], 'string'],
            [['balance'], 'number'],
            [['access_token', 'email', 'password'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BinpartnerAccountParams::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);


        $query->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password]);

        DateHelper::addQueryByDateRange($query, self::tableName() . '.created_at', $this->created_at);
        DateHelper::addQueryByDateRange($query, self::tableName() . '.updated_at', $this->updated_at);

        return $dataProvider;
    }
}
