<?php

namespace app\models\binpartner\account;

use app\models\BaseModel;
use app\service\binpartner\AbstractStatisticReaderService;
use app\service\binpartner\StatisticReaderService;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "binpartner_account_params".
 *
 * @property int $id
 * @property string|null $access_token
 * @property string $email
 * @property string|null $password
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property float|null $balance
 */
class BinpartnerAccountParams extends BaseModel
{
    const STATUS_DISABLE = 0;
    const STATUS_ENABLE = 1;
    const CHECK_PROFILE_METHOD = 'stats/common/day/parse';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'binpartner_account_params';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'unique'],
            ['email', 'email'],
            [
                'status',
                'in',
                'range' =>
                    array_keys(self::getStatusList())

            ],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['email', 'password'], 'string', 'max' => 255],
            [['access_token'], 'string', 'max' => 512],
            [['balance'], 'number'],
            ['access_token', 'validateAccessToken'],
        ];
    }

    public function validateAccessToken($attribute, $params)
    {
        $params['user_token'] = $this->access_token;
        $params['period_from'] = date(date("Y-m-d", strtotime("-100 day")));//"2022-02-25"
        $params['period_to'] = date('Y-m-d'); //"2022-02-25"
        if ($this->status != self::STATUS_DISABLE) {
            $data = (new StatisticReaderService())->getDataByParams($params, self::CHECK_PROFILE_METHOD);
            if ($data === false) {
                $this->addError($attribute, 'You entered an invalid access token.');
            }
        }
    }

    /**
     * @return array
     */
    public static function getActiveAccountList(): array
    {
        return ArrayHelper::map(self::findall(['status' => self::STATUS_ENABLE]),'id','email');
    }

    /**
     * @return array
     */
    public static function getStatusList(): array
    {
        return [
            self::STATUS_ENABLE => Yii::t('app', 'enable'),
            self::STATUS_DISABLE => Yii::t('app', 'disable'),
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllActiveAccount()
    {
        return self::find()->where(['status' => self::STATUS_ENABLE])->all();
    }

    /**
     * @return string
     */
    public function getNameStatus()
    {
        return Yii::t('app', $this->status);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_token' => 'Access Token',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'balance' => 'Balance',
        ];
    }
}
