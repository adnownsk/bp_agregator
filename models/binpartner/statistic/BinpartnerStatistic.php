<?php

namespace app\models\binpartner\statistic;

use app\models\BaseModel;
use app\models\binpartner\account\BinpartnerAccountParams;

/**
 * This is the model class for table "binpartner_statistic".
 *
 * @property int $id
 * @property int|null $binpartner_account_params_id
 * @property string $day
 * @property int|null $visitors
 * @property int|null $reg
 * @property int|null $conf
 * @property float|null $ctr
 * @property int|null $demo
 * @property int|null $ftd
 * @property float|null $ftds
 * @property float|null $first_ratio
 * @property float|null $sign_up_ratio
 * @property int|null $deposits_count
 * @property float|null $deposits
 * @property int|null $traders
 * @property int|null $deals_count
 * @property float|null $deals
 * @property float|null $flows
 * @property float|null $withs
 * @property float|null $profit
 * @property string $ratio
 *
 * @property BinpartnerAccountParams $binpartnerAccountParams
 */
class BinpartnerStatistic extends BaseModel
{
    public $ratio;
    public const LOG_PREFIX = 'StatisticReader';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'binpartner_statistic';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['binpartner_account_params_id', 'visitors', 'reg', 'conf', 'demo', 'ftd', 'deposits_count', 'traders', 'deals_count', 'created_at'], 'integer'],
            [['day'], 'safe'],
            [['ratio'], 'safe'],
            [['ctr', 'ftds', 'first_ratio', 'sign_up_ratio', 'deposits', 'deals', 'flows', 'withs', 'profit'], 'number'],
            [['binpartner_account_params_id'], 'exist', 'skipOnError' => true, 'targetClass' => BinpartnerAccountParams::className(), 'targetAttribute' => ['binpartner_account_params_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'binpartner_account_params_id' => 'Binpartner Account Params ID',
            'day' => 'Day',
            'visitors' => 'Visitors',
            'reg' => 'Reg',
            'conf' => 'Conf',
            'ctr' => 'Ctr',
            'demo' => 'Demo',
            'ftd' => 'Ftd',
            'ratio' => 'Ratio',
            'ftds' => 'Ftds',
            'first_ratio' => 'First Ratio',
            'sign_up_ratio' => 'Sign Up Ratio',
            'deposits_count' => 'Deposits Count',
            'deposits' => 'Deposits',
            'traders' => 'Traders',
            'deals_count' => 'Deals Count',
            'created_at' => 'Created at',
            'deals' => 'Deals',
            'flows' => 'Flows',
            'withs' => 'Withs',
            'profit' => 'Profit',
        ];
    }

    /**
     * Gets query for [[BinpartnerAccountParams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBinpartnerAccountParams()
    {
        return $this->hasOne(BinpartnerAccountParams::className(), ['id' => 'binpartner_account_params_id']);
    }

    public static function updateOrCreateStatistic($params)
    {
        $params = self::getValidParams($params);
        $attributes = [
            'binpartner_account_params_id' => $params['binpartner_account_params_id'],
            'day' => $params['day']
        ];
        return self::updateOrCreate($attributes, $params);
    }

    /**
     * @param int $binPartnerAccountId
     * @param array $data
     * @return bool
     */
    public static function getValidParams(array $data): array
    {
        return [
            'binpartner_account_params_id' => (int)$data['binpartner_account_params_id'],
            'day' => isset($data['day']) ? strtotime($data['day']) : null,
            'visitors' => isset($data['visitors']) ? (int)$data['visitors'] : null,
            'reg' => isset($data['regs']) ? (int)$data['regs'] : null,
            'conf' => isset($data['confs']) ? (int)$data['confs'] : null,
            'ctr' => isset($data['ctr']) ? (float)$data['ctr'] : null,
            'demo' => isset($data['demo']) ? (int)$data['demo'] : null,
            'ftd' => isset($data['ftd']) ? (int)$data['ftd'] : null,
            'ftds' => isset($data['ftds']) ? (float)$data['ftds'] : null,
            'first_ratio' => isset($data['ftd_ratio']) ? (float)$data['ftd_ratio'] : null,
            'sign_up_ratio' => isset($data['reg_ratio']) ? (float)$data['reg_ratio'] : null,
            'deposits_count' => isset($data['deposits']) ? (int)$data['deposits'] : null,
            'deposits' => isset($data['sum_deposits']) ? (float)$data['sum_deposits'] : null,
            'traders' => isset($data['traders']) ? (int)$data['traders'] : null,
            'deals_count' => isset($data['deals']) ? (int)$data['deals'] : null,
            'deals' => isset($data['sum_deals']) ? (float)$data['sum_deals'] : null,
            'flows' => isset($data['flows']) ? (float)$data['flows'] : null,
            'withs' => isset($data['withdrawals']) ? (float)$data['withdrawals'] : null,
            'profit' => isset($data['profit']) ? (float)$data['profit'] : null,
        ];
    }


}
