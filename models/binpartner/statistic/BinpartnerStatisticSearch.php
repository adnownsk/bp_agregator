<?php

namespace app\models\binpartner\statistic;

use app\components\DateHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\binpartner\statistic\BinpartnerStatistic;

/**
 * BinpartnerStatisticSearch represents the model behind the search form of `app\models\binpartner\statistic\BinpartnerStatistic`.
 */
class BinpartnerStatisticSearch extends BinpartnerStatistic
{
    public $pageSize;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'pageSize', 'binpartner_account_params_id', 'visitors', 'reg', 'conf', 'demo', 'ftd', 'deposits_count', 'traders', 'deals_count'], 'integer'],
            [['day'], 'safe'],
            [['ctr', 'ftds', 'first_ratio', 'sign_up_ratio', 'deposits', 'deals', 'flows', 'withs', 'profit'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BinpartnerStatistic::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'binpartner_account_params_id',
                    'day',
                    'visitors',
                    'reg',
                    'conf',
                    'ctr',
                    'demo',
                    'ftd',
                    'ftds',
                    'ratio' => [
                        'asc' => ['first_ratio' => SORT_ASC, 'sign_up_ratio' => SORT_ASC],
                        'desc' => ['first_ratio' => SORT_DESC, 'sign_up_ratio' => SORT_DESC],
                        'label' => 'Ratio',
                    ],
                    'deposits_count',
                    'deposits',
                    'traders',
                    'deals_count',
                    'created_at',
                    'deals',
                    'flows',
                    'withs',
                    'profit'
                ],
            ],
            'pagination' => [
                'pageSize' => Yii::$app->request->get('pageSize', 25),
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'binpartner_account_params_id' => $this->binpartner_account_params_id,
            'visitors' => $this->visitors,
            'reg' => $this->reg,
            'conf' => $this->conf,
            'ctr' => $this->ctr,
            'demo' => $this->demo,
            'ftd' => $this->ftd,
            'ftds' => $this->ftds,
            'first_ratio' => $this->first_ratio,
            'sign_up_ratio' => $this->sign_up_ratio,
            'deposits_count' => $this->deposits_count,
            'deposits' => $this->deposits,
            'traders' => $this->traders,
            'deals_count' => $this->deals_count,
            'deals' => $this->deals,
            'flows' => $this->flows,
            'withs' => $this->withs,
            'profit' => $this->profit,
        ]);

        DateHelper::addQueryByDateRange($query, self::tableName() . '.day', $this->day);

        return $dataProvider;
    }
}
