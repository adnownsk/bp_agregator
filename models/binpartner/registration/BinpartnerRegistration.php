<?php

namespace app\models\binpartner\registration;

use app\models\BaseModel;
use app\models\binpartner\account\BinpartnerAccountParams;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "binpartner_registration".
 *
 * @property int $id
 * @property int|null $binpartner_account_params_id
 * @property int|null $trader_id
 * @property string|null $campaign
 * @property string|null $subaccount
 * @property int|null $registration_date
 * @property string|null $country
 * @property int|null $created_at
 *
 * @property BinpartnerAccountParams $binpartnerAccountParams
 */
class BinpartnerRegistration extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'binpartner_registration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['binpartner_account_params_id', 'trader_id', 'registration_date', 'created_at'], 'integer'],
            [['campaign', 'subaccount', 'country'], 'string', 'max' => 255],
            [['binpartner_account_params_id'], 'exist', 'skipOnError' => true, 'targetClass' => BinpartnerAccountParams::className(), 'targetAttribute' => ['binpartner_account_params_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'binpartner_account_params_id' => Yii::t('app', 'Binpartner Account Params ID'),
            'trader_id' => Yii::t('app', 'Trader ID'),
            'campaign' => Yii::t('app', 'Campaign'),
            'subaccount' => Yii::t('app', 'Subaccount'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'country' => Yii::t('app', 'Country'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Gets query for [[BinpartnerAccountParams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBinpartnerAccountParams()
    {
        return $this->hasOne(BinpartnerAccountParams::className(), ['id' => 'binpartner_account_params_id']);
    }

    public static function updateOrCreateRegistration($params)
    {
        $params = self::getValidParams($params);
        $attributes = [
            'binpartner_account_params_id' => $params['binpartner_account_params_id'],
            'trader_id' => $params['trader_id'],
            'registration_date' => $params['registration_date']
        ];
        return self::updateOrCreate($attributes, $params);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function getValidParams(array $data): array
    {
        return [
            'binpartner_account_params_id' => (int)$data['binpartner_account_params_id'],
            'trader_id' => isset($data['trader_id']) ? (float)$data['trader_id'] : null,
            'campaign' => $data['campaign'] ?? null,
            'subaccount' => $data['subaccount'] ?? null,
            'registration_date' => isset($data['registration_date']) ? strtotime($data['registration_date']) : null,
            'country' => $data['country'] ?? null,
        ];
    }

    /**
     * @return array
     */
    public static function getParamList($param): array
    {
        return ArrayHelper::map(self::find()->select([$param])->all(), $param, $param);
    }
}
