<?php

namespace app\models\binpartner\registration;

use app\components\DateHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\binpartner\registration\BinpartnerRegistration;

/**
 * BinpartnerRegistrationSearch represents the model behind the search form of `app\models\binpartner\registration\BinpartnerRegistration`.
 */
class BinpartnerRegistrationSearch extends BinpartnerRegistration
{
    public $pageSize;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','pageSize', 'binpartner_account_params_id', 'trader_id', 'created_at'], 'integer'],
            [['campaign', 'subaccount', 'country', 'registration_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BinpartnerRegistration::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->request->get('pageSize', 25),
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'binpartner_account_params_id' => $this->binpartner_account_params_id,
            'trader_id' => $this->trader_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'campaign', $this->campaign])
            ->andFilterWhere(['like', 'subaccount', $this->subaccount])
            ->andFilterWhere(['like', 'country', $this->country]);

        DateHelper::addQueryByDateRange($query, self::tableName() . '.registration_date', $this->registration_date);
        return $dataProvider;
    }
}
